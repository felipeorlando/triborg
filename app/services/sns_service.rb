# frozen_string_literal: true

class SnsService
  def initialize(operation, publisher: client)
    @operation = operation
    @publisher = publisher
  end

  def publish(object)
    json = object.is_a?(Hash) ? object.to_json : object

    @publisher.publish message: json, topic_arn: topic_for
  end

  private

  def client
    (Rails.env.production? ? Aws::SNS::Client : RedisService).new
  end

  def topic_for
    Config::Sns.topic_for @operation
  end
end
