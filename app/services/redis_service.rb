# frozen_string_literal: true

class RedisService
  def initialize(pool: RedisPoolService.default)
    @redis = pool
  end

  def publish(topic_arn:, message:)
    write topic_arn, message
  end

  private

  def write(key, object)
    return if object.nil?

    json = object.is_a?(Hash) ? object.to_json : object

    @redis.rpush key, json

    key
  end
end
