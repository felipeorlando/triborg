class ComponentRenderer
  def initialize(body)
    @body = body
  end

  def render(context = nil)
    @body_rendered ||= begin
      components.each do |component|
        instance = component.instance

        @body.gsub! instance.to_s, instance.render(context)
      end

      @body
    end
  end

  def components
    @components ||= matches.collect { |m| ComponentWrapper.new m[0], m[1] }.uniq
  end

  private

  def matches
    (@body || "").scan Component::Componentable::TAG_PATTERN
  end
end
