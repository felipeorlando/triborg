require "connection_pool"

class RedisPoolService
  class << self
    def default
      pool
    end

    private

    def pool(host: Config::Redis.host, port: Config::Redis.port.to_i, db: Config::Redis.db.to_i)
      ConnectionPool::Wrapper.new(size: Config::Redis.pool.size.to_i, timeout: Config::Redis.pool.timeout.to_i) do
        Redis.new host: host, port: port, db: db
      end
    end
  end
end
