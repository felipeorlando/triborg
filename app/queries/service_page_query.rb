class ServicePageQuery
  def initialize(relation = Page::Service.all)
    @relation = relation
  end

  def find_to_show(root_category_slug, slug)
    @relation
      .joins(:category)
      .where(
        categories: {
          slug: root_category_slug
        }
      )
      .friendly
      .find slug
  end
end
