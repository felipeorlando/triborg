class InternalLinkImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  process :store_file_info

  def store_dir
    "uploads/#{model.class.to_s.underscore}/image/#{model.id}"
  end

  def store_file_info
    return unless @file
    return unless model

    model.image = SecureRandom.hex
  end

  def filename
    return unless original_filename

    if model && model.read_attribute(:image).present?
      model.read_attribute(:image)
    else
      @name ||= "#{SecureRandom.hex}.#{file.extension}"
    end
  end

  private

  def full_filename_for
    "#{model.header_image_filename}.#{model.header_image_extension}"
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

end
