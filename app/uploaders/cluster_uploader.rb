# encoding: utf-8

class ClusterUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  process :store_file_info

  def store_dir
    "uploads/#{model.class.to_s.underscore}/icon_file_name/#{model.id}"
  end

  def store_file_info
    return unless @file
    return unless model

    model.icon = SecureRandom.hex
    model.icon_file_extension = @file.extension
  end

  def filename
    return unless original_filename

    if model && model.read_attribute(:icon).present?
      model.read_attribute(:icon)
    else
      @name ||= "#{SecureRandom.hex}.#{file.extension}"
    end
  end

  private

  def full_filename_for
    "#{model.header_image_filename}.#{model.header_image_extension}"
  end

  def extension_white_list
    %w(svg)
  end
end
