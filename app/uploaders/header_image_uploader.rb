class HeaderImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  process quality: 90
  process :store_file_info

  def store_dir
    "uploads/#{model.class.name.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def filename
    return unless original_filename

    if model && model.read_attribute(:header_image).present?
      model.read_attribute(:header_image)
    else
      @name ||= "#{SecureRandom.hex}.#{file.extension}"
    end
  end

  version :_1920x560 do
    process resize_to_fill: [1920, 560]
    process quality: 75

    def full_filename(_for_file = model.header_image.file)
      full_filename_for "1920x560"
    end
  end

  version :_1440x560, from_version: :_1920x560 do
    process resize_to_fill: [1440, 560]
    process quality: 75

    def full_filename(_for_file = model.header_image.file)
      full_filename_for "1440x560"
    end
  end

  version :_1024x560, from_version: :_1440x560 do
    process resize_to_fill: [1024, 560]
    process quality: 75

    def full_filename(_for_file = model.header_image.file)
      full_filename_for "1024x560"
    end
  end

  version :_768x320, from_version: :_1024x560 do
    process resize_to_fill: [768, 320]
    process quality: 75

    def full_filename(_for_file = model.header_image.file)
      full_filename_for "768x320"
    end
  end

  def store_file_info
    return unless @file
    return unless model

    model.header_image_filename = SecureRandom.hex
    model.header_image_extension = @file.extension
  end

  private

  def full_filename_for(version)
    "#{model.header_image_filename}_#{version}.#{model.header_image_extension}"
  end
end
