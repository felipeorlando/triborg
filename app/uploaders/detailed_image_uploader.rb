# encoding: utf-8

class DetailedImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  process quality: 90
  process :store_file_info

  def store_dir
    "uploads/#{model.class.name.underscore}/#{mounted_as}/#{model.id}"
  end

  version :_720x340 do
    process resize_to_fill: [720, 340]
    process quality: 75

    def full_filename(_for_file = model.image.file)
      full_filename_for "720x340"
    end
  end

  version :_360x170, from_version: :_720x340 do
    process resize_to_fill: [360, 170]

    def full_filename(_for_file = model.image.file)
      full_filename_for "360x170"
    end
  end

  def store_file_info
    return unless @file
    return unless model

    model.image_filename = SecureRandom.hex
    model.image_extension = @file.extension
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def filename
    return unless original_filename

    if model && model.read_attribute(:image).present?
      model.read_attribute(:image)
    else
      @name ||= "#{SecureRandom.hex}.#{file.extension}"
    end
  end

  private

  def full_filename_for(version)
    "#{model.image_filename}_#{version}.#{model.image_extension}"
  end
end
