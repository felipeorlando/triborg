//= require ckeditor/init
//= require ckeditor/config
//= require jquery.grideditor

$(document).on('ready pjax:success', function() {
  $('.grid-editor-field .controls').each(function (index, el) {
    var $el = $(el);
    var gridContainerClass = 'grid-editor-container-' + index;

    $el.addClass('grid-wrapper-' + index);

    $el.append('<div class="' + gridContainerClass + '"></div>');

    $el.find('.' + gridContainerClass).gridEditor({
      source_textarea: '.grid-wrapper-' + index + ' textarea.form-control',
      new_row_layouts: [[12], [6,6], [9,3]],
      row_classes: [],
      col_classes: [],
      content_types: ['ckeditor'],
      ckeditor: {
        config: window.CKEDITOR.config
      }
    });
  });

  $('.content form').submit(function (e) {
    $('.grid-editor-field .controls').each(function (index, el) {
      var $el = $(el);
      var gridContainerClass = '.grid-editor-container-' + index;
      var html = $el.find(gridContainerClass).gridEditor('getHtml');

      $el.find('textarea').val(html);
    });
  });
});
