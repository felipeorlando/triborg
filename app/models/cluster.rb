class Cluster < ApplicationRecord
  has_many :pages, class_name: "Page::Service"

  acts_as_paranoid

  has_paper_trail ignore: [:created_at, :updated_at]

  mount_uploader :icon, ClusterUploader

  validates :name,
    presence: true,
    length: { maximum: 50 }

  validates :alt,
    presence: true,
    length: { maximum: 255 }

  validates :icon,
    presence: true
end
