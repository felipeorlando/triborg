module Component
  class Citation < ApplicationRecord
    include Componentable

    strip_attributes(
      collapse_spaces: true,
      replace_newlines: true,
      only: [:name, :info]
    )

    validates :name,
      length: { maximum: 100 }

    validates :info,
      length: { maximum: 100 }

    validates :body,
      presence: true

    def tag_containers
      [:body]
    end
  end
end
