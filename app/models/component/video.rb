# frozen_string_literal: true

module Component
  class Video < ApplicationRecord
    include Componentable

    acts_as_paranoid

    has_paper_trail ignore: [:created_at, :updated_at]

    attr_writer :url

    URL_FORMATS = {
      youtube: [
        %r(https?://youtu\.be/(.+)),
        %r(https?://www\.youtube\.com/watch\?v=(.*?)(&|#|$)),
        %r(https?://www\.youtube\.com/embed/(.*?)(\?|$)),
        %r(https?://www\.youtube\.com/v/(.*?)(#|\?|$)),
        %r(https?://www\.youtube\.com/user/.*?#\w/\w/\w/\w/(.+)\b)
      ]
    }

    strip_attributes(
      collapse_spaces: true,
      replace_newlines: true,
      only: [:video_id]
    )

    validates :video_id,
      length: { maximum: 20 },
      presence: true

    validates :url, presence: true

    def url
      @url = mount_url
    end

    def url=(url)
      if url.nil?
        @url, self.video_id = nil
        return
      end

      @url = mount_url(url)
      self.video_id = extract_video_id(@url)
    end

    def tag_containers
      []
    end

    private
    def extract_video_id(url)
      url_formats = URL_FORMATS[:youtube]

      url_formats.find { |format| url =~ format } and $1
    end

    def mount_url(url = @url)
      return "https://www.youtube.com/embed/#{extract_video_id(url)}" unless url.nil?

      "https://www.youtube.com/embed/#{video_id}" unless video_id.nil?
    end
  end
end
