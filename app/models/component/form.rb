# frozen_string_literal: true

module Component
  class Form < ApplicationRecord
    include Componentable

    belongs_to :category

    acts_as_paranoid

    has_paper_trail ignore: [:created_at, :updated_at]

    validates :category, presence: true

    DEFAULT_CSS = "cms-widget.css"

    def css_file
      self[:css_file].presence || DEFAULT_CSS
    end

    def tag_containers
      []
    end
  end
end
