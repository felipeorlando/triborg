# frozen_string_literal: true

module Component
  class InternalLink < ApplicationRecord
    include Componentable

    mount_uploader :image, InternalLinkImageUploader

    acts_as_paranoid

    has_paper_trail ignore: [:created_at, :updated_at]

    strip_attributes(
      collapse_spaces: true,
      replace_newlines: true,
      only: [:title, :url]
    )

    validates :title, :url, :image, presence: true
    validates :title, length: { maximum: 45 }
    validates :ref, length: { maximum: 100 }

    def tag_containers
      []
    end
  end
end
