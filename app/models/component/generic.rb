module Component
  class Generic < ApplicationRecord
    include Componentable

    def title
      ref
    end

    def tag_containers
      [:body]
    end
  end
end
