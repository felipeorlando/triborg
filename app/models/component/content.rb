module Component
  class Content < ApplicationRecord
    include Componentable

    enum backgrounds: [
      :blank,
      :sky
    ]

    strip_attributes(
      collapse_spaces: true,
      replace_newlines: true,
      only: [:title, :subtitle]
    )

    validates :title,
      length: { maximum: 255 }

    validates :subtitle,
      length: { maximum: 255 }

    def tag_containers
      [:body]
    end
  end
end
