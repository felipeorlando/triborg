module Component
  class Header < ApplicationRecord
    include Componentable

    mount_uploader :header_image, HeaderImageUploader

    strip_attributes(
      collapse_spaces: true,
      replace_newlines: true,
      only: [:title, :subtitle]
    )

    validates :title,
      length: { maximum: 255 }

    validates :subtitle,
      length: { maximum: 255 }

    def image
      @image ||= if header_image.present?
                   header_image
                 elsif context.category.present?
                   context.category.header_image
                 end
    end

    def tag_containers
      []
    end
  end
end
