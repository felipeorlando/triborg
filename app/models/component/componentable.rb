# frozen_string_literal: true

module Component
  module Componentable
    COMPONENT_NAME_PATTERN = "([a-zA-Z0-9]+)"

    REF_LENGTH = 1..100
    REF_PATTERN = "[a-z0-9\\/-]{#{REF_LENGTH.first},#{REF_LENGTH.end}}"
    REF_PROP_PATTERN = " ref:(#{REF_PATTERN})"

    TAG_PATTERN = /\[#{COMPONENT_NAME_PATTERN}#{REF_PROP_PATTERN}\]/

    attr_accessor :context

    def self.included(base)
      base.include Relationshipable

      base.acts_as_paranoid

      base.has_paper_trail ignore: [:created_at, :updated_at]

      base.validates :ref,
        presence: true,
        uniqueness: true,
        format: { with: /\A#{REF_PATTERN}\z/ },
        length: { within: REF_LENGTH }

      base.validate :ref_can_be_changed_validation,
        on: :update,
        if: :ref_changed?
    end

    def render(context = nil)
      self.context = context

      html = ApplicationController.renderer.render(
        file: Rails.root.join("app/views/#{partial_path}").to_s,
        layout: nil,
        locals: {
          object: self
        }
      )

      ComponentRenderer.new(html).render context
    end

    def partial_path
      @partial_path ||= class_name.gsub("::", "/").underscore
    end

    def component_name
      @component_name ||= class_name.gsub "#{Component}::", ""
    end

    def to_s
      "[#{component_name} ref:#{ref}]"
    end

    private

    def class_name
      self.class.name
    end

    def ref_can_be_changed_validation
      return unless used_by_another_object?

      errors.add :ref,
        "não pode ser alterado por estar sendo referenciado em outro objeto."
    end
  end
end
