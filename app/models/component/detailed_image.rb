# frozen_string_literal: true

module Component
  class DetailedImage < ApplicationRecord
    include Componentable

    mount_uploader :image, DetailedImageUploader

    acts_as_paranoid

    has_paper_trail ignore: [:created_at, :updated_at]

    strip_attributes(
      collapse_spaces: true,
      replace_newlines: true,
      only: [:title, :description]
    )

    validates :title, :description, :image, presence: true
    validates :title, length: { maximum: 100 }
    validates :description, length: { maximum: 500 }

    def tag_containers
      [:description]
    end
  end
end
