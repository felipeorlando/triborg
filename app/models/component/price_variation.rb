# frozen_string_literal: true

module Component
  class PriceVariation < ApplicationRecord
    include Componentable

    acts_as_paranoid

    has_paper_trail ignore: [:created_at, :updated_at]

    strip_attributes(
      collapse_spaces: true,
      replace_newlines: true,
      only: [:title]
    )

    validates :title,
      length: { maximum: 100 },
      presence: true

    validates :cheap,
      numericality: { less_than: :expensive }

    validates :average,
      numericality: { less_than: :expensive, greater_than: :cheap }

    validates :expensive,
      numericality: { greater_than: :cheap }

    def tag_containers
      []
    end
  end
end
