class Relationship < ApplicationRecord
  belongs_to :from_relatable, polymorphic: true, inverse_of: :from_relations
  belongs_to :to_relatable, polymorphic: true, inverse_of: :to_relations
end
