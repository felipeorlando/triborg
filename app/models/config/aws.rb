module Config
  class Aws < Settingslogic
    source Rails.root.join("config/aws.yml")

    namespace Rails.env
  end
end
