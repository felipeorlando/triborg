# frozen_string_literal: true

module Config
  class Sns < Settingslogic
    source Rails.root.join("config/sns.yml")

    namespace Rails.env

    def topic_for(operation)
      [subdomain, region, Config::Aws.id, topics[operation]].join ":"
    end
  end
end
