module Config
  class Redis < Settingslogic
    source Rails.root.join("config/redis.yml")

    namespace Rails.env
  end
end
