class Category < ApplicationRecord
  acts_as_tree cache_depth: true

  has_many :service_categories, class_name: "::Page::ServiceCategory"
  has_many :service_pages, through: :service_categories, source: :service

  mount_uploader :header_image, HeaderImageUploader

  validates :slug,
    presence: true,
    length: { maximum: 255 }

  validates :name,
    presence: true,
    length: { maximum: 255 }

  validates :friendly_name,
    presence: true,
    length: { maximum: 255 }
end
