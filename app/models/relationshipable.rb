module Relationshipable
  def self.included(base)
    base.has_many :from_relations,
      as: :from_relatable,
      class_name: "Relationship"

    base.has_many :to_relations,
      as: :to_relatable,
      class_name: "Relationship"

    base.after_save :build_relations

    base.validate :component_existence_validation

    base.class_eval do
      def destroy
        if Relationship.exists?(to_relatable: self)
          raise "Erro. Componente sendo referenciado em outro objeto."
        end

        Relationship.where(from_relatable: self).destroy_all

        super
      end
    end
  end

  def tag_containers
    raise NotImplementedError
  end

  private

  def tag_container_values
    tag_containers.map { |container| self[container] }
  end

  def build_relations
    return unless tag_containers.any?

    build_removed
    build_new
    detect_circular_reference to_s, self
  end

  def components
    @components ||= begin
      ComponentRenderer.new(tag_container_values.join).components.uniq
    end
  end

  def instances
    @instances ||= components.map(&:instance)
  end

  def build_removed
    from_relations.each do |relation|
      relation.destroy unless instances.include?(relation.to_relatable)
    end
  end

  def build_new
    instances.each do |instance|
      Relationship.find_or_create_by(
        from_relatable: self,
        to_relatable: instance
      )
    end
  end

  def component_existence_validation
    component_not_found = components.find { |c| !c.found? }

    return if component_not_found.blank?

    message = "componente \"#{component_not_found.name}\"" \
      " (ref: \"#{component_not_found.ref}\") não encontrado."

    errors.add :base, message
  end

  def used_by_another_object?
    Relationship.exists? to_relatable: self
  end

  def detect_circular_reference(root_tag, item)
    parents(item).each do |parent|
      parent.to_s == root_tag && raise("Erro. Referência circular detectada.")

      detect_circular_reference root_tag, parent
    end
  end

  def parents(item = self)
    item.to_relations.map(&:from_relatable)
  end
end
