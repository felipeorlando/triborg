module Page
  module Renderer
    def render_field(field_content, context = nil)
      ComponentRenderer.new(field_content).render context
    end
  end
end
