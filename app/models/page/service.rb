module Page
  class Service < ApplicationRecord
    include Pageable

    extend FriendlyId

    after_update :purge

    validate :category_should_be_root

    has_many :service_categories, class_name: "::Page::ServiceCategory"
    has_many :categories, through: :service_categories

    belongs_to :category
    belongs_to :cluster

    acts_as_paranoid
    has_paper_trail ignore: %i[created_at updated_at]
    friendly_id :slug_candidates, use: %i[slugged history]

    strip_attributes(
      collapse_spaces: true,
      replace_newlines: true,
      only: %i[
        title
        meta_description
        meta_keywords
        friendly_name
        short_name
      ]
    )

    validates :category,
      presence: true

    validates :cluster,
      presence: true

    validates :title,
      presence: true,
      length: { maximum: 255 }

    validates :friendly_name,
      presence: true,
      length: { maximum: 255 }

    validates :meta_description,
      length: { maximum: 255 }

    validates :meta_keywords,
      length: { maximum: 255 }

    def purge
      SnsService.new(:purge).publish url: purge_url
    end

    def purge_url
      url_helpers.show_service_url(category.slug,
        slug: slug,
        host: Config::AppSettings.host)
    end

    def to_param
      slug
    end

    def tag_containers
      %i[header internal_link content sidebar]
    end

    private

    def category_should_be_root
      errors.add(:category, :non_root) unless category&.root?
    end

    def slug_candidates
      [
        :friendly_name,
        %i[friendly_name slug_disambiguation]
      ]
    end

    def slug_disambiguation
      SecureRandom.hex 3
    end

    def should_generate_new_friendly_id?
      friendly_name_changed?
    end

    def url_helpers
      Rails.application.routes.url_helpers
    end
  end
end
