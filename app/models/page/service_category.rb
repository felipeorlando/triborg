module Page
  class ServiceCategory < ApplicationRecord
    belongs_to :service, class_name: "::Page::Service"
    belongs_to :category, class_name: "::Category"
  end
end
