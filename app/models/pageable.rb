module Pageable
  def self.included(base)
    base.include Page::Renderer
    base.include Relationshipable
  end
end
