class ServicesController < ApplicationController
  def show
    @page = ServicePageQuery.new.find_to_show params[:root_category_slug],
      params[:slug]

    render_template_fields

    push_assets
  end

  private

  def push_assets
    path = ActionController::Base.helpers.asset_path "application.css"

    response.headers["Link"] = "<#{path}>; rel=preload; as=style\n"
  end

  def page_context
    @page_context ||= begin
      context = Component::Context.new
      context.category = @page.category
      context
    end
  end

  def render_template_fields
    @header        = @page.render_field @page.header, page_context
    @internal_link = @page.render_field @page.internal_link, page_context
    @content       = @page.render_field @page.content, page_context
    @sidebar       = @page.render_field @page.sidebar, page_context
  end
end
