ComponentWrapper = Struct.new(:name, :ref) do
  def instance
    @instance ||= constant&.find_by ref: ref
  end

  def found?
    instance.present?
  end

  private

  def constant
    "#{Component}::#{name}".constantize
  rescue NameError
    nil
  end
end
