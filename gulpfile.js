var gulp = require("gulp");

gulp.task("default", function() {
  "use strict";

  var
    rename  = require("gulp-rename"),
    entries = [
      ["node_modules/airbrake-js/dist/client.js", "airbrake.js"],
      ["node_modules/gaiden/dist/gaiden.min.css.map"],
      ["node_modules/gaiden/dist/gaiden.min.css"]
    ];

  function dest(filename) {
    if (/\.(css|sass|scss|map)$/.test(filename)) {
      return "app/assets/stylesheets";
    }

    if (/\.(js)$/.test(filename)) {
      return "app/assets/javascripts";
    }
  }

  for (var i = 0; i < entries.length; i++) {
    var
      file     = entries[i][0],
      filename = entries[i][1] || file.split("/").pop();

    gulp
    .src(file)
    .pipe(rename(filename))
    .pipe(gulp.dest(dest(filename)));
  }
});
