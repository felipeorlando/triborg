require "rails_helper"

describe Component::InternalLink do
  subject { build :component_internal_link }

  it_behaves_like "componentable",
    "InternalLink",
    "component/internal_link"

  describe "validate" do
    describe "ref" do
      it { is_expected.to validate_presence_of(:ref) }
      it { is_expected.to validate_uniqueness_of(:ref) }
      it { is_expected.to validate_length_of(:ref).is_at_most 100 }
    end

    describe "title" do
      it { is_expected.to validate_presence_of(:title) }
      it { is_expected.to validate_length_of(:title).is_at_most 45 }
    end

    describe "url" do
      it { is_expected.to validate_presence_of(:url) }
    end
  end
end
