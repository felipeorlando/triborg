require "rails_helper"

describe Component::Header do
  subject { build :component_header }

  it_behaves_like "componentable",
    "Header",
    "component/header"

  describe "validate" do
    describe "title" do
      it { is_expected.to validate_length_of(:title).is_at_most 255 }
    end

    describe "subtitle" do
      it { is_expected.to validate_length_of(:subtitle).is_at_most 255 }
    end
  end
end
