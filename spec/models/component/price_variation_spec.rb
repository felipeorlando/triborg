require "rails_helper"

describe Component::PriceVariation do
  subject { build :component_price_variation }

  it_behaves_like "componentable",
    "PriceVariation",
    "component/price_variation"

  describe "validate" do
    describe "title" do
      it { is_expected.to validate_presence_of(:title) }
      it { is_expected.to validate_length_of(:title).is_at_most 100 }
    end

    describe "cheap" do
      it do
        is_expected.to validate_numericality_of(:cheap)
          .is_less_than subject.expensive
      end
    end

    describe "average" do
      it do
        is_expected.to validate_numericality_of(:average)
          .is_less_than(subject.expensive)
          .is_greater_than(subject.cheap)
      end
    end

    describe "expensive" do
      it do
        is_expected.to validate_numericality_of(:expensive)
          .is_greater_than subject.cheap
      end
    end
  end
end
