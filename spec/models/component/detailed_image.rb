require "rails_helper"

describe Component::DetailedImage do
  subject { build :component_detailed_image }

  it_behaves_like "componentable",
    "DetailedImage",
    "component/detailed_image"

  it_behaves_like "componentable_with_tag_containers"
  it_behaves_like "relationshipable_with_tag_containers"

  describe "validate" do
    describe "title" do
      it { is_expected.to validate_presence_of(:title) }
      it { is_expected.to validate_length_of(:title).is_at_most 100 }
    end

    describe "description" do
      it { is_expected.to validate_presence_of(:description) }
    end

    describe "image" do
      it { is_expected.to validate_presence_of(:image) }
    end
  end
end
