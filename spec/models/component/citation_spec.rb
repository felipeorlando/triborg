require "rails_helper"

describe Component::Citation do
  subject { build :component_citation }

  it_behaves_like "componentable",
    "Citation",
    "component/citation"

  it_behaves_like "componentable_with_tag_containers"
  it_behaves_like "relationshipable_with_tag_containers"

  describe "validate" do
    describe "name" do
      it { is_expected.to validate_length_of(:name).is_at_most 100 }
    end

    describe "info" do
      it { is_expected.to validate_length_of(:info).is_at_most 100 }
    end

    describe "body" do
      it { is_expected.to validate_presence_of :body }
    end
  end
end
