require "rails_helper"

describe Component::Video do
  subject { build :component_video }

  it_behaves_like "componentable",
    "Video",
    "component/video"

  describe "validate" do
    describe "video_id" do
      it { is_expected.to validate_presence_of(:video_id) }
      it { is_expected.to validate_length_of(:video_id).is_at_most 20 }
      it { is_expected.to validate_presence_of(:url) }
    end
  end

  describe "before save" do
    describe "video_id" do
      it { is_expected.to_not be nil }
    end
  end
end
