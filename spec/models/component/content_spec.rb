require "rails_helper"

describe Component::Content do
  subject { build :component_content }

  it_behaves_like "componentable",
    "Content",
    "component/content"

  it_behaves_like "componentable_with_tag_containers"
  it_behaves_like "relationshipable_with_tag_containers"

  describe "validate" do
    describe "title" do
      it { is_expected.to validate_length_of(:title).is_at_most 255 }
    end

    describe "subtitle" do
      it { is_expected.to validate_length_of(:subtitle).is_at_most 255 }
    end
  end
end
