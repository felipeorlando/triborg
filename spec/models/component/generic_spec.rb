require "rails_helper"

describe Component::Generic do
  subject { build :component_generic }

  it_behaves_like "componentable",
    "Generic",
    "component/generic"

  it_behaves_like "componentable_with_tag_containers"
  it_behaves_like "relationshipable_with_tag_containers"
end
