require "rails_helper"

describe Component::Form do
  subject { build :component_form }

  it_behaves_like "componentable", "Form", "component/form"

  describe "validate" do
    describe "category" do
      it { is_expected.to validate_presence_of(:category) }
    end
  end
end
