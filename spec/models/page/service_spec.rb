require "rails_helper"

describe Page::Service do
  subject { build :page_service }

  it_behaves_like "relationshipable_with_tag_containers"

  describe "validate" do
    describe "title" do
      it { is_expected.to validate_presence_of :title }

      it { is_expected.to validate_length_of(:title).is_at_most 255 }
    end

    describe "friendly_name" do
      it { is_expected.to validate_presence_of :friendly_name }

      it { is_expected.to validate_length_of(:friendly_name).is_at_most 255 }
    end

    describe "meta_description" do
      it { is_expected.to validate_length_of(:meta_description).is_at_most 255 }
    end

    describe "meta_keywords" do
      it { is_expected.to validate_length_of(:meta_keywords).is_at_most 255 }
    end

    describe "category" do
      it { is_expected.to validate_presence_of :category }

      context "with a non root category" do
        let(:root_category) { create :category }
        let(:subcategory)   { create :category, parent: root_category }

        before do
          subject.category = subcategory
          subject.save
        end

        it do
          expect(subject.errors[:category]).to include I18n.t "activerecord." \
            "errors.models.page/service.attributes.category.non_root"
        end
      end
    end
  end
end
