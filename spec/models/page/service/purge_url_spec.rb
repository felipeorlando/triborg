require "rails_helper"

describe Page::Service, ".purge_url" do
  let!(:category) { build :category, slug: :category_slug }
  let!(:page)     { build :page_service, category: category, slug: :page_slug }

  it "mounts the production url used for purge" do
    expected = "http://localhost:3000/category_slug/servicos/page_slug"

    expect(page.purge_url).to eq expected
  end
end
