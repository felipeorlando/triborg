require "rails_helper"

describe Page::Service, ".purge_after_update" do
  context "on save" do
    let!(:page) { build :page_service }

    it "does not triggers" do
      expect(page).to_not receive(:purge)

      page.save!
    end
  end

  context "on update" do
    let!(:page) { create :page_service }

    it "triggers" do
      expect(page).to receive(:purge).once

      page.save!
    end
  end
end
