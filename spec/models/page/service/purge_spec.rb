require "rails_helper"

describe Page::Service, ".purge" do
  let!(:category)    { build :category, slug: :category_slug }
  let!(:page)        { build :page_service, category: category, slug: :page_slug }
  let!(:sns_service) { double SnsService }

  before do
    expect(SnsService).to receive(:new).with(:purge) { sns_service }
  end

  it "purges the page cache" do
    expect(sns_service).to receive(:publish).with(url: page.purge_url)

    page.purge
  end
end
