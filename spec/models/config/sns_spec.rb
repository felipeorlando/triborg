# frozen_string_literal: true

require "rails_helper"

RSpec.describe Config::Sns, "#topic_for" do
  let!(:source) do
    {
      "region"    => "region",
      "subdomain" => "arn:aws:sns",
      "topics" => {
        "purge" => "purge.value"
      }
    }
  end

  before { allow(Config::Aws).to receive(:id) { "id" } }

  subject { described_class.new source }

  it "builds an arn endpoint" do
    expect(subject.topic_for(:purge)).to eq "arn:aws:sns:region:id:purge.value"
  end
end
