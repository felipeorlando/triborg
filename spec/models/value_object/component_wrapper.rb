require "rails_helper"

describe ComponentWrapper do
  describe "#instance" do
    context "with an existing component" do
      let(:component) { create :component_dummy }

      context "and an existing reference" do
        let(:wrapper) do
          build :component_wrapper, name: "Dummy", ref: component.ref
        end

        it "finds the component object" do
          expect(wrapper.instance).to eq component
        end
      end

      context "and an non existing reference" do
        let(:wrapper) do
          build :component_wrapper, name: "Dummy", ref: "non-existing"
        end

        it "doesn't find a component object" do
          expect(wrapper.instance).to eq nil
        end
      end
    end

    context "without an existing component" do
      let(:wrapper) { build :component_wrapper, name: "Nothing", ref: "none" }

      it "doesn't find a component object" do
        expect(wrapper.instance).to eq nil
      end
    end
  end

  describe "#found?" do
    context "when there's an instance" do
      let(:component) { create :component_dummy }
      let(:wrapper) do
        build :component_wrapper, name: "Dummy", ref: component.ref
      end

      before do
        allow(wrapper).to receive(:instance).and_return [component]
      end

      it "finds it" do
        expect(wrapper.found?).to match true
      end
    end

    context "when there's not an instance" do
      let(:wrapper) { build :component_wrapper, name: "Nothing", ref: "none" }

      before do
        allow(wrapper).to receive(:instance).and_return nil
      end

      it "doesn't find it" do
        expect(wrapper.found?).to match false
      end
    end
  end
end
