require "rails_helper"

describe Cluster do
  subject { build :cluster }

  specify { is_expected.to be_versioned }

  specify { is_expected.to act_as_paranoid }

  describe "validate" do
    describe "name" do
      it { is_expected.to validate_presence_of(:name) }
      it { is_expected.to validate_length_of(:name).is_at_most 50 }
    end

    describe "alt" do
      it { is_expected.to validate_presence_of(:alt) }
      it { is_expected.to validate_length_of(:alt).is_at_most 255 }
    end

    describe "icon" do
      it { is_expected.to validate_presence_of(:icon) }
    end
  end
end
