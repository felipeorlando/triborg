FactoryGirl.define do
  factory :component_video, class: Component::Video do
    sequence(:ref) { |n| "ref-#{n}" }
    sequence(:video_id) { |n| "video#{n}" }
  end
end
