FactoryGirl.define do
  factory :component_generic, class: Component::Generic do
    sequence(:ref) { |n| "ref-#{n}" }
  end
end
