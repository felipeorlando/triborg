FactoryGirl.define do
  factory :category do
    sequence(:name) { |n| "Name #{n}" }
    sequence(:friendly_name) { |n| "Friendly Name #{n}" }
    sequence(:slug) { |n| "friendly-name-#{n}" }
  end
end
