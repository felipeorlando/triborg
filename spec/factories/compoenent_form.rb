FactoryGirl.define do
  factory :component_form, class: Component::Form do
    association :category
    sequence(:ref) { |n| "ref-#{n}" }
  end
end
