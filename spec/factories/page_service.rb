FactoryGirl.define do
  factory :page_service, class: Page::Service do
    association :category
    association :cluster
    sequence(:title) { |n| "Title #{n}" }
    sequence(:friendly_name) { |n| "Friendly name #{n}" }
  end
end
