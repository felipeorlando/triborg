FactoryGirl.define do
  factory :component_detailed_image, class: Component::DetailedImage do
    sequence(:ref) { |n| "ref-#{n}" }
    sequence(:title) { |n| "title-#{n}" }
    sequence(:description) { |n| "description-#{n}" }
    image do
      Rack::Test::UploadedFile.new(
        Rails.root.join("spec/fixtures/images/detailed_image.png")
      )
    end
  end
end
