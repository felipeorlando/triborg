FactoryGirl.define do
  factory :component_content, class: Component::Content do
    sequence(:ref) { |n| "ref-#{n}" }
  end
end
