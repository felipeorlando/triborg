FactoryGirl.define do
  factory :component_header, class: Component::Header do
    sequence(:ref) { |n| "ref-#{n}" }
  end
end
