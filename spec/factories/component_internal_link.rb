FactoryGirl.define do
  factory :component_internal_link, class: Component::InternalLink do
    sequence(:ref) { |n| "ref-#{n}" }
    sequence(:title) { |n| "title-#{n}" }
    sequence(:url) { |n| "http://getninjas.com.br/#{n}" }
    image do
      Rack::Test::UploadedFile.new(
        Rails.root.join("spec/fixtures/images/internal_link.png")
      )
    end
  end
end
 