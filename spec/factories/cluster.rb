FactoryGirl.define do
  factory :cluster do
    sequence(:name) { |n| "name-#{n}" }
    sequence(:alt) { |n| "alt-#{n}" }
    icon do
      Rack::Test::UploadedFile.new(
        Rails.root.join("spec/fixtures/images/eventos.svg")
      )
    end
  end
end
