FactoryGirl.define do
  factory :component_price_variation, class: Component::PriceVariation do
    sequence(:ref) { |n| "ref-#{n}" }
    sequence(:title) { |n| "Price title #{n}" }
    sequence(:cheap) { |n| n }
    sequence(:average) { |n| 1 + n }
    sequence(:expensive) { |n| 10 + n }
  end
end
