FactoryGirl.define do
  factory :component_citation, class: Component::Citation do
    sequence(:ref) { |n| "ref-#{n}" }
    sequence(:body) { |n| "Citation body #{n}" }
  end
end
