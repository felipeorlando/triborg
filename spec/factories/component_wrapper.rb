FactoryGirl.define do
  factory :component_wrapper, class: ComponentWrapper do
    sequence(:name) { |n| "name- #{n}" }
    sequence(:ref) { |n| "ref-#{n}" }
  end
end
