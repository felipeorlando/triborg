module Component
  class Dummy < ApplicationRecord
    include Component::Componentable

    def tag_containers
      [:body]
    end

    def partial_path
      "../../spec/support/dummy_component_view.html.erb"
    end
  end
end

FactoryGirl.define do
  factory :component_dummy, class: Component::Dummy do
    sequence(:ref) { |n| "ref-#{n}" }
  end
end
