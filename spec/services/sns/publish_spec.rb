# frozen_string_literal: true

require "rails_helper"

RSpec.describe SnsService, ".publish" do
  let!(:object)    { { key: "value" } }
  let!(:operation) { "purge" }

  context "on a development environment" do
    let!(:publisher) { double RedisService }

    before do
      allow(RedisService).to receive(:new) { publisher }
    end

    context "with no injected publisher" do
      subject { described_class.new operation }

      it "publishes a json on redis" do
        expect(publisher).to receive(:publish).with(message: object.to_json, topic_arn: "arn:aws:sns:us-east-1:590284024382:purges") { true }

        expect(subject.publish(object)).to eq true
      end
    end

    context "with injected publisher" do
      let!(:publisher) { double }

      subject { described_class.new operation, publisher: publisher }

      it "publishes a json on given publisher" do
        expect(publisher).to receive(:publish).with(message: object.to_json, topic_arn: "arn:aws:sns:us-east-1:590284024382:purges") { true }

        expect(subject.publish(object)).to eq true
      end
    end
  end

  context "on a production environment" do
    let!(:publisher) { double Aws::SNS::Client }

    before do
      allow(Aws::SNS::Client).to receive(:new) { publisher }
      allow(Rails.env).to receive(:production?) { true }
    end

    context "with no injected publisher" do
      subject { described_class.new operation }

      it "publishes a json on sns via returned url" do
        expect(publisher).to receive(:publish).with(message: object.to_json, topic_arn: "arn:aws:sns:us-east-1:590284024382:purges") { true }

        expect(subject.publish(object)).to eq true
      end
    end

    context "with injected publisher" do
      let!(:publisher) { double }

      subject { described_class.new operation, publisher: publisher }

      it "publishes a json on given publisher" do
        expect(publisher).to receive(:publish).with(message: object.to_json, topic_arn: "arn:aws:sns:us-east-1:590284024382:purges") { true }

        expect(subject.publish(object)).to eq true
      end
    end
  end
end
