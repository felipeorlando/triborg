# frozen_string_literal: true

require "rails_helper"

RSpec.describe RedisService, ".publish", :redis do
  let!(:topic) { :topic }

  context "and a nil object" do
    let!(:object) { nil }

    subject { described_class.new.publish topic_arn: topic, message: object }

    it "skips the write" do
      subject

      expect(redis.keys).to eq []
    end

    specify { expect(subject).to be_nil }
  end

  context "and an empty hash object" do
    let!(:object) { {} }

    subject { described_class.new.publish(topic_arn: topic, message: object) }

    it "works normally" do
      subject

      expect(redis.rpop(topic)).to eq "{}"
    end

    it "returns the given topic" do
      expect(subject).to eq topic
    end
  end

  context "and a valid hash object" do
    let!(:object) { { name: :wbotelhos } }

    subject { described_class.new.publish(topic_arn: topic, message: object) }

    it "adds a json object into the given topic" do
      subject

      expect(redis.rpop(topic)).to eq object.to_json
    end

    it "returns the given topic" do
      expect(subject).to eq topic
    end
  end

  context "and a valid json object" do
    let!(:object) { { name: :wbotelhos }.to_json }

    subject do
      described_class.new.publish(topic_arn: topic, message: object.to_json)
    end

    it "adds a json object into the given topic" do
      subject

      expect(redis.rpop(topic)).to eq object.to_json
    end

    it "returns the given topic" do
      expect(subject).to eq topic
    end
  end
end
