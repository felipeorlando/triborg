# frozen_string_literal: true

require "rails_helper"

RSpec.describe RedisPoolService, ".default", :redis do
  it "returns a redis instance" do
    pool = described_class.default

    expect(pool).to be_instance_of Redis
  end

  xit "returns a default redis config" do
  end

  xit "returns a pool with right config" do
  end
end
