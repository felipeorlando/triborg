require "rails_helper"

describe ComponentRenderer do
  describe "#render" do
    context "with an empty body" do
      subject { ComponentRenderer.new "" }

      it "returns an empty string" do
        expect(subject.render).to eq ""
      end
    end

    context "with some components in its body" do
      let(:comp_1) { create :component_dummy, ref: "ref-a", body: "A" }

      let(:comp_2) { create :component_dummy, ref: "ref-b", body: "B" }

      subject { ComponentRenderer.new "#{comp_1}-#{comp_2}" }

      it "returns the body with components rendered" do
        expect(subject.render).to include <<~HTML
          <div data-ref="ref-a">
            A
          </div>
          -
          <div data-ref="ref-b">
            B
          </div>
        HTML
      end
    end

    context "with components that have another components" do
      let!(:inner_comp) do
        create :component_dummy, ref: "inner", body: "inner component"
      end

      let!(:outer_comp) do
        create :component_dummy,
          ref: "outer",
          body: "outer component and:[Dummy ref:inner]"
      end

      subject { ComponentRenderer.new "components:#{outer_comp}" }

      it "renders the entire tree" do
        expect(subject.render).to include <<~HTML
          components:
          <div data-ref="outer">
            outer component and:
          <div data-ref="inner">
            inner component
          </div>

          </div>
        HTML
      end
    end

    context "when the same component tag appears multiple times" do
      let!(:comp) { create :component_dummy, ref: "ref", body: "Capiroto" }

      subject { ComponentRenderer.new "#{comp}+#{comp}-#{comp}" }

      it "renders multiple times" do
        expect(subject.render).to include <<~HTML
          <div data-ref="ref">
            Capiroto
          </div>
          +
          <div data-ref="ref">
            Capiroto
          </div>
          -
          <div data-ref="ref">
            Capiroto
          </div>
        HTML
      end
    end
  end

  describe "#components" do
    subject { ComponentRenderer.new "" }

    context "with an empty body" do
      it "returns an empty array" do
        expect(subject.components).to eq []
      end
    end

    context "with some components in its body" do
      subject { ComponentRenderer.new "[Comp1 ref:abc] - [Comp2 ref:123]" }

      it "returns a structured array with these components" do
        expect(subject.components).to match_array(
          [
            ComponentWrapper.new("Comp1", "abc"),
            ComponentWrapper.new("Comp2", "123")
          ]
        )
      end
    end

    context "with two mentions of the same component" do
      let(:component_tag) { "[Comp ref:abc]" }

      subject { ComponentRenderer.new "#{component_tag} - #{component_tag}" }

      it "returns only one" do
        expect(subject.components).to eq(
          [ComponentWrapper.new("Comp", "abc")]
        )
      end
    end
  end
end
