require "rails_helper"

feature "Admin / Component / InternalLink" do
  scenario "navigate to creation screen" do
    visit rails_admin_path

    within ".sidebar-nav" do
      click_on "Links internos"
    end

    click_on "Criar novo"

    expect(current_path).to eq rails_admin.new_path(
      model_name: "component~internal_link"
    )
  end

  describe "CRUD" do
    let(:image_path) do
      Component::InternalLink.last.image.url
    end

    context "with new price component" do
      before do
        visit rails_admin.new_path model_name: "component~internal_link"

        within ".content" do
          fill_in "Ref", with: "ninjeiro"
          fill_in "Título", with: "Profissa"
          fill_in "Url", with: "http://getninjas.com.br"
          attach_file "Image",
            Rails.root.join("spec/fixtures/images/internal_link.png")
          
          click_on "Gravar"
        end
      end

      scenario "component is created" do
        click_on "Mostrar"

        within ".content" do
          expect(page).to have_text "ninjeiro"
          expect(page).to have_text "Profissa"
          expect(page).to have_text "http://getninjas.com.br"
          expect(page).to have_selector "img[src='#{image_path}']"
        end
      end

      context "in a service page" do
        given!(:category) { create :category, slug: "ninja" }
        given!(:service_page) do
          create :page_service,
            friendly_name: "Furtivo",
            category: category,
            internal_link: "[InternalLink ref:ninjeiro]"
        end

        scenario "presents component" do
          visit "/ninja/servicos/furtivo"

          expect(page).to have_text "Profissa"
          expect(page).to have_selector "a[href='http://getninjas.com.br']"
          expect(page).to have_selector "img[src='#{image_path}']"
        end
      end
    end
  end
end
