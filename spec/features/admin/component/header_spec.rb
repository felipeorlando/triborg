require "rails_helper"

feature "Admin / Component / Header" do
  scenario "navigate to creation screen" do
    visit rails_admin_path

    within ".sidebar-nav" do
      click_on "Cabeçalhos"
    end

    click_on "Criar novo"

    expect(current_path).to eq rails_admin.new_path(
      model_name: "component~header"
    )
  end

  describe "CRUD" do
    let(:header_image_path) do
      Component::Header.last.header_image.url :_1920x560
    end

    context "with new header component" do
      before do
        visit rails_admin.new_path model_name: "component~header"

        within ".content" do
          fill_in "Ref", with: "comp-ref"
          fill_in "Título", with: "Título maroto"
          fill_in "Subtítulo", with: "Subtitle maroto"
          attach_file "Image",
            Rails.root.join("spec/fixtures/images/header_image.jpg")

          click_on "Gravar"
        end
      end

      scenario "component is created" do
        click_on "Mostrar"

        within ".content" do
          expect(page).to have_text "comp-ref"
          expect(page).to have_text "Título maroto"
          expect(page).to have_text "Subtitle maroto"
          expect(page).to have_selector "img[src='#{header_image_path}']"
        end
      end

      context "in a service page" do
        given(:category) { create :category, slug: "ninja" }
        given!(:service_page) do
          create :page_service,
            friendly_name: "Furtivo",
            category: category,
            content: "[Header ref:comp-ref]"
        end

        scenario "presents component" do
          visit "/ninja/servicos/furtivo"

          expect(page).to have_text "Título maroto"
          expect(page).to have_text "Subtitle maroto"
        end
      end
    end
  end
end
