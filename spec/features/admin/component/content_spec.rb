require "rails_helper"

feature "Admin / Component / Content" do
  scenario "navigate to creation screen" do
    visit rails_admin_path

    within ".sidebar-nav" do
      click_on "Conteúdos"
    end

    click_on "Criar novo"

    expect(current_path).to eq rails_admin.new_path(
      model_name: "component~content"
    )
  end

  describe "CRUD" do
    context "with new content component" do
      before do
        visit rails_admin.new_path model_name: "component~content"

        within ".content" do
          fill_in "Ref", with: "comp-ref"
          fill_in "Título", with: "Título maroto"
          fill_in "Subtítulo", with: "Subtitle maroto"
          fill_in "Conteúdo", with: "Body maroto!"

          click_on "Gravar"
        end
      end

      scenario "component is created" do
        click_on "Mostrar"

        within ".content" do
          expect(page).to have_text "comp-ref"
          expect(page).to have_text "Título maroto"
          expect(page).to have_text "Subtitle maroto"
          expect(page).to have_text "Body maroto!"
        end
      end

      context "in a service page" do
        given(:category) { create :category, slug: "ninja" }
        given!(:service_page) do
          create :page_service,
            friendly_name: "parkour",
            category: category,
            content: "[Content ref:comp-ref]"
        end

        scenario "presents component" do
          visit "/ninja/servicos/parkour"

          expect(page).to have_text "Título maroto"
          expect(page).to have_text "Subtitle maroto"
          expect(page).to have_text "Body maroto!"
        end
      end
    end
  end
end
