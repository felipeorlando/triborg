require "rails_helper"

feature "Admin / Component / Generic" do
  scenario "navigate to creation screen" do
    visit rails_admin_path

    within ".sidebar-nav" do
      click_on "Genéricos"
    end

    click_on "Criar novo"

    expect(current_path).to eq rails_admin.new_path(
      model_name: "component~generic"
    )
  end

  describe "CRUD" do
    context "with new generic component" do
      before do
        visit rails_admin.new_path model_name: "component~generic"

        within ".content" do
          fill_in "Ref", with: "comp-ref"
          fill_in "Conteúdo", with: "Body maroto!"

          click_on "Gravar"
        end
      end

      scenario "component is created" do
        click_on "Mostrar"

        within ".content" do
          expect(page).to have_text "comp-ref"
          expect(page).to have_text "Body maroto!"
        end
      end

      context "in a service page" do
        given(:category) { create :category, slug: "ninja" }
        given!(:service_page) do
          create :page_service,
            friendly_name: "assasino",
            category: category,
            content: "[Generic ref:comp-ref]"
        end

        scenario "presents component" do
          visit "/ninja/servicos/assasino"

          expect(page).to have_text "Body maroto!"
        end
      end
    end
  end
end
