require "rails_helper"

feature "Admin / Component / DetailedImage" do
  scenario "navigate to creation screen" do
    visit rails_admin_path

    within ".sidebar-nav" do
      click_on "Imagens com detalhe"
    end

    click_on "Criar novo"

    expect(current_path).to eq rails_admin.new_path(
      model_name: "component~detailed_image"
    )
  end

  describe "CRUD" do
    before { skip "Too much intermittence using fill_in_ckeditor" }

    context "with new detailed image component", :js do
      let(:detailed_image) do
        Component::DetailedImage.last
      end

      let(:detailed_image_path) do
        detailed_image.image.url(:_720x340)
      end

      before do
        visit rails_admin.new_path model_name: "component~detailed_image"

        within ".content" do
          fill_in "Ref", with: "otima-ref-para-image"
          fill_in "Título", with: "que imagem linda!"
          fill_in_ckeditor "component_detailed_image_description",
            with: "essa image e uma foto muito bonita"
          attach_file "Imagem",
            Rails.root.join("spec/fixtures/images/detailed_image.png")

          click_on "Gravar"
        end
      end

      scenario "component is created" do
        visit rails_admin.show_path model_name: "component~detailed_image",
                                    id: detailed_image.id

        within ".content" do
          expect(page).to have_text "otima-ref-para-image"
          expect(page).to have_text "que imagem linda!"
          expect(page).to have_text "essa image e uma foto muito bonita"
          expect(page).to have_selector "img[src='#{detailed_image_path}']"
        end
      end

      context "in a service page" do
        let(:detailed_image_path) do
          Component::DetailedImage.last.image.url(:_360x170)
        end

        given(:category) { create :category, slug: "ninja" }
        given!(:service_page) do
          create :page_service,
            friendly_name: "liquid",
            category: category,
            content: "[DetailedImage ref:otima-ref-para-image]"
        end

        scenario "presents component" do
          visit "/ninja/servicos/liquid"

          expect(page).to have_text "que imagem linda!"
          expect(page).to have_text "essa image e uma foto muito bonita"
          expect(page)
            .to have_xpath("//img[contains(@src,'#{detailed_image_path}')]")
        end
      end
    end
  end
end
