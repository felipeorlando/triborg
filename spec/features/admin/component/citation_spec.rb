require "rails_helper"

feature "Admin / Component / Citation" do
  scenario "navigate to creation screen" do
    visit rails_admin_path

    within ".sidebar-nav" do
      click_on "Citações"
    end

    click_on "Criar novo"

    expect(current_path).to eq rails_admin.new_path(
      model_name: "component~citation"
    )
  end

  describe "CRUD" do
    context "with new citaion component" do
      before do
        visit rails_admin.new_path model_name: "component~citation"

        within ".content" do
          fill_in "Ref", with: "ninja-ref"
          fill_in "Nome", with: "Nome do Ninja"
          fill_in "Informações", with: "Info ninjisticas"
          fill_in "Conteúdo", with: "Body sarado de ninja"

          click_on "Gravar"
        end
      end

      scenario "component is created" do
        click_on "Mostrar"

        within ".content" do
          expect(page).to have_text "ninja-ref"
          expect(page).to have_text "Nome do Ninja"
          expect(page).to have_text "Info ninjisticas"
          expect(page).to have_text "Body sarado de ninja"
        end
      end

      context "in a service page" do
        given(:category) { create :category, slug: "ninja" }
        given!(:service_page) do
          create :page_service,
            friendly_name: "Segurança",
            category: category,
            content: "[Citation ref:ninja-ref]"
        end

        scenario "presents component" do
          visit "/ninja/servicos/seguranca"

          expect(page).to have_text "Nome do Ninja"
          expect(page).to have_text "Info ninjisticas"
          expect(page).to have_text "Body sarado de ninja"
        end
      end
    end
  end
end
