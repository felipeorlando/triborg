require "rails_helper"

feature "Admin / Component / Video" do
  scenario "navigate to creation screen" do
    visit rails_admin_path

    within ".sidebar-nav" do
      click_on "Vídeo"
    end

    click_on "Criar novo"

    expect(current_path).to eq rails_admin.new_path(
      model_name: "component~video"
    )
  end

  describe "CRUD" do
    context "with new video component" do
      before do
        visit rails_admin.new_path model_name: "component~video"

        within ".content" do
          fill_in "Ref", with: "video-ref"
          fill_in "Url do vídeo", with: "https://www.youtube.com/watch?v=csDMGXtTzbM"

          click_on "Gravar"
        end
      end

      scenario "component is created" do
        click_on "Mostrar"

        within ".content" do
          expect(page).to have_text "video-ref"
          expect(page).to have_text "csDMGXtTzbM"
        end
      end

      context "in a service page" do
        given(:category) { create :category, slug: "ninja" }
        given!(:service_page) do
          create :page_service,
            friendly_name: "parkour",
            category: category,
            content: "[Video ref:video-ref]"
        end

        scenario "presents component" do
          visit "/ninja/servicos/parkour"

          within ".component" do
            expect(page.find("iframe.video-player")[:src]).to eq "https://www.youtube.com/embed/csDMGXtTzbM"
          end
        end
      end
    end

    scenario "invalid url" do
      visit rails_admin.new_path model_name: "component~video"

      within ".content" do
        fill_in "Ref", with: "video-ref"
        fill_in "Url do vídeo", with: "https://www.google.com/"

        click_on "Gravar"
      end

      expect(page.find(".alert")).to have_text "vídeo criado sem sucesso"
      expect(page.find(".alert")).to have_text "- Video não pode ficar em branco"
    end
  end
end
