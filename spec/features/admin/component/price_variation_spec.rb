require "rails_helper"

feature "Admin / Component / PriceVariation" do
  scenario "navigate to creation screen" do
    visit rails_admin_path

    within ".sidebar-nav" do
      click_on "Variação de preços"
    end

    click_on "Criar novo"

    expect(current_path).to eq rails_admin.new_path(
      model_name: "component~price_variation"
    )
  end

  describe "CRUD" do
    context "with new price component" do
      before do
        visit rails_admin.new_path model_name: "component~price_variation"

        within ".content" do
          fill_in "Ref", with: "ninja-price-table-ref"
          fill_in "Título", with: "valor Ninja"
          fill_in "Barato", with: "500.50"
          fill_in "Médio", with: "3000.00"
          fill_in "Caro", with: "50000"

          click_on "Gravar"
        end
      end

      scenario "component is created" do
        click_on "Mostrar"

        within ".content" do
          expect(page).to have_text "ninja-price-table-ref"
          expect(page).to have_text "valor Ninja"
          expect(page).to have_text "500.5"
          expect(page).to have_text "3000.0"
          expect(page).to have_text "3000.0"
        end
      end

      context "in a service page" do
        given(:category) { create :category, slug: "ninja" }
        given!(:service_page) do
          create :page_service,
            friendly_name: "Invisível",
            category: category,
            content: "[PriceVariation ref:ninja-price-table-ref]"
        end

        scenario "presents component" do
          visit "/ninja/servicos/invisivel"

          within "header" do
            expect(page).to have_text "valor Ninja"
          end

          within ".price-variation" do
            expect(page).to have_text "R$ 500,50"
            expect(page).to have_text "R$ 3.000,00"
            expect(page).to have_text "R$ 3.000,00"
          end
        end
      end
    end
  end
end
