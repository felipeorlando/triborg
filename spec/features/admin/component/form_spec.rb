require "rails_helper"

feature "Admin / Component / Form" do
  scenario "navigate to creation screen" do
    visit rails_admin_path

    within ".sidebar-nav" do
      click_on "Formulário"
    end

    click_on "Criar novo"

    expect(current_path).to eq rails_admin.new_path(
      model_name: "component~form"
    )
  end

  describe "CRUD" do
    given!(:category) { create :category }

    context "with new form component", :js do
      before do
        visit rails_admin.new_path model_name: "component~form"

        within ".content" do
          fill_in "Ref", with: "ninja-ref"
          fill_in "Arquivo CSS", with: "ninja.css"
          fill_autocomplete_select("component_form_category_id", index: 0)

          click_on "Gravar"
        end
      end

      scenario "component is created" do
        click_on "Mostrar"

        within ".content" do
          expect(page).to have_text("ninja-ref")
            .and have_text("ninja.css")
            .and have_link category.slug
        end
      end

      context "in a service page" do
        given(:category) { create :category, slug: "ninja" }
        given!(:service_page) do
          create :page_service,
            friendly_name: "Segurança",
            category: category,
            content: "[Form ref:ninja-ref]"
        end

        scenario "presents component" do
          visit "/ninja/servicos/seguranca"

          expect(page).to have_css "#gn-request-form-widget"
        end
      end
    end
  end
end
