require "rails_helper"

feature "Admin / Page / Service" do
  scenario "navigate to creation screen" do
    visit rails_admin_path

    within ".sidebar-nav" do
      click_on "Serviços"
    end

    click_on "Criar novo"

    expect(current_path).to eq rails_admin.new_path model_name: "page~service"
  end

  describe "CRUD" do
    given!(:category) { create :category }
    given!(:cluster) { create :cluster }

    scenario "creates a new service page", :js do
      visit rails_admin.new_path model_name: "page~service"

      within ".content" do
        fill_in "Slug", with: "Slug Maroto"
        fill_in "Título", with: "Título Maroto"
        fill_in "Meta description", with: "Meta desc Marota"
        fill_in "Meta keywords", with: "Meta key Marota"
        fill_autocomplete_select("page_service_category_id", index: 0)
        fill_autocomplete_select("page_service_cluster_id", index: 1)

        click_on "Gravar"
      end

      click_on "Mostrar"

      within ".content" do
        expect(page).to have_text "slug-maroto"
        expect(page).to have_text "Título Maroto"
        expect(page).to have_text "Meta desc Marota"
        expect(page).to have_text "Meta key Marota"
        expect(page).to have_link category.slug
        expect(page).to have_link cluster.name
      end
    end
  end

  describe "when there is a service page" do
    given(:category) { create :category, slug: "ninja" }
    given(:cluster) { create :cluster, name: "novo cluster" }
    given!(:service_page) do
      create :page_service,
        friendly_name: "Invisível",
        category: category,
        cluster: cluster,
        title: "gran Ninja"
    end

    scenario "render service page" do
      visit "/ninja/servicos/invisivel"

      expect(page).to have_title "gran Ninja"
    end
  end
end
