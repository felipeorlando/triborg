RSpec.configure do |config|
  config.filter_run_when_matching :focus

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.order = :random

  Kernel.srand config.seed

  config.after(:suite) do
    FileUtils.rm_rf Dir["#{Rails.root}/public/uploads/tmp"]
  end
end
