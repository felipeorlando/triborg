require "rails_helper"

describe ServicesController, "GET show" do
  let(:root_category_slug) { "eventos" }

  let(:slug) { "capirotisse" }

  context "with valid params" do
    let!(:category) { create :category, slug: root_category_slug }

    let!(:service) do
      create :page_service, category: category, friendly_name: slug
    end

    specify do
      get :show, params: { root_category_slug: root_category_slug, slug: slug }

      expect(response).to be_success
    end

    context "when rename the service slug" do
      let(:new_slug) { "capirotisse-doida" }

      before do
        service.slug = new_slug
        service.save!
      end

      context "with the old slug" do
        specify do
          get :show, params: {
            root_category_slug: root_category_slug,
            slug: slug
          }

          expect(response).to be_success
        end
      end

      context "with the new slug" do
        specify do
          get :show, params: {
            root_category_slug: root_category_slug,
            slug: new_slug
          }

          expect(response).to be_success
        end
      end
    end
  end

  context "with nonexistent category slug" do
    let!(:service) { create :page_service, friendly_name: slug }

    specify do
      expect do
        get :show, params: { root_category_slug: "xxx", slug: slug }
      end.to raise_error ActionController::RoutingError
    end
  end

  context "with category slug that does not correspond to that service slug" do
    let!(:category) { create :category, slug: root_category_slug }

    let!(:category_xxx) { create :category, slug: "xxx" }

    let!(:service) do
      create :page_service, category: category, friendly_name: slug
    end

    specify do
      expect do
        get :show, params: { root_category_slug: category_xxx.slug, slug: slug }
      end.to raise_error ActionController::RoutingError
    end
  end
end
