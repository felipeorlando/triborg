# frozen_string_literal: true

module FakeRedisHelper
  require "fakeredis/rspec"

  def redis
    RedisPoolService.default
  end
end

RSpec.configure do |config|
  config.include FakeRedisHelper, :redis
end
