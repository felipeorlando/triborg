require "rails_helper"

RSpec.shared_examples "relationshipable" do
  describe "relations" do
    it { is_expected.to have_many(:from_relations).class_name "Relationship" }

    it { is_expected.to have_many(:to_relations).class_name "Relationship" }
  end

  it "raises an exception when 'tag_containers' method is not implemented" do
    expect { subject.tag_containers }.to_not raise_error
  end

  context "with a component that is referenced by another component" do
    let(:component) { build :component_dummy, body: subject.to_s }

    before do
      subject.save
      component.save
    end

    it "refuses deletion" do
      expect { subject.destroy }.to raise_error(
        RuntimeError,
        "Erro. Componente sendo referenciado em outro objeto."
      )
    end
  end
end
