require "rails_helper"

RSpec.shared_examples "componentable" do |name, path|
  it_behaves_like "relationshipable"

  specify { is_expected.to be_versioned }

  specify { is_expected.to act_as_paranoid }

  describe "validate" do
    describe "ref" do
      let(:allowed_values) { ["a", "a-a", "1", "abc/123-"] }

      let(:not_allowed_values) { ["X", "a a", "a\\1", "é", "x.x"] }

      it { is_expected.to validate_presence_of :ref }

      it { is_expected.to validate_uniqueness_of :ref }

      it { is_expected.to validate_length_of(:ref).is_at_most 100 }

      it { is_expected.to allow_values(*allowed_values).for :ref }

      it { is_expected.to_not allow_values(*not_allowed_values).for :ref }

      context "when the component is not used by another object" do
        before do
          subject.save

          allow(Relationship).to receive(:exists?).with(to_relatable: subject)
            .and_return false

          subject.ref = "new-ref-value"
          subject.save
        end

        it "allow ref to be changed" do
          expect(subject.valid?).to eq true
        end
      end

      context "when the component is used by another object" do
        before do
          subject.save

          allow(Relationship).to receive(:exists?).with(to_relatable: subject)
            .and_return true

          subject.ref = "new-ref-value"
          subject.save
        end

        it "do not allow ref to be changed" do
          expect(subject.errors.messages).to include(
            ref: ["não pode ser alterado por estar sendo referenciado" \
              " em outro objeto."]
          )
        end
      end
    end
  end

  describe "#render" do
    let(:context) { double }

    let(:application_render_params) do
      {
        file: Rails.root.join("app/views/#{path}").to_s,
        layout: nil,
        locals: {
          object: subject
        }
      }
    end

    let(:application_renderer) { double render: nil }

    let(:application_render_result) { double }

    let(:component_renderer) { double render: nil }

    let(:component_render_result) { double }

    before do
      allow(ApplicationController).to receive(:renderer)
        .and_return application_renderer

      allow(application_renderer).to receive(:render)
        .with(application_render_params).and_return application_render_result

      allow(ComponentRenderer).to receive(:new).with(application_render_result)
        .and_return component_renderer

      allow(component_renderer).to receive(:render).with(context)
        .and_return component_render_result
    end

    it "returns the result from ComponentRenderer#render method" do
      expect(subject.render(context)).to eq component_render_result
    end

    it "set the context attribute" do
      subject.render context

      expect(subject.context).to eq context
    end
  end

  describe "#component_name" do
    specify { expect(subject.component_name).to eq name }
  end

  describe "#partial_path" do
    specify { expect(subject.partial_path).to eq path }
  end

  describe "#to_s" do
    let(:ref) { "ref-test" }

    before { subject.ref = ref }

    specify { expect(subject.to_s).to eq "[#{name} ref:#{ref}]" }
  end
end
