require "rails_helper"

RSpec.shared_examples "componentable_with_tag_containers" do
  context "after save" do
    context "referencing itself in a tag container" do
      let(:subject_reloaded) { subject.class.find subject.id }

      before do
        subject.save

        subject_reloaded[subject_reloaded.tag_containers.first] \
          = subject_reloaded.to_s
      end

      it "raises an exception to prevent circular reference" do
        expect { subject_reloaded.save }.to raise_error(
          "Erro. Referência circular detectada."
        )
      end
    end

    context "referencing an parent component" do
      let(:comp_root) { create :component_dummy, body: comp_sub.to_s }

      let(:comp_sub) { create :component_dummy, body: subject.to_s }

      let(:subject_reloaded) { subject.class.find subject.id }

      before do
        subject.save

        subject_reloaded[subject_reloaded.tag_containers.first] \
          = comp_root.to_s
      end

      it "raises an exception to prevent circular reference" do
        expect { subject_reloaded.save }.to raise_error(
          "Erro. Referência circular detectada."
        )
      end
    end
  end
end
