require "rails_helper"

RSpec.shared_examples "relationshipable_with_tag_containers" do
  context "after save" do
    context "without any component on its tag container" do
      context "adding one component on its tag container" do
        let(:component) { create :component_dummy }

        let(:tag) { component.to_s }

        before do
          subject[subject.tag_containers.first] = tag
          subject.save!
        end

        it "creates a record in the relationship tree" do
          expect(
            Relationship.exists?(
              from_relatable: subject,
              to_relatable: component
            )
          ).to eq true
        end

        context "and when the component is removed" do
          let(:subject_reloaded) { subject.class.find subject.id }

          before do
            subject_reloaded[subject_reloaded.tag_containers.first] \
              = "New content."
            subject_reloaded.save!
          end

          it "removes the relationship too" do
            expect(
              Relationship.exists?(
                from_relatable: subject_reloaded,
                to_relatable: component
              )
            ).to eq false
          end
        end
      end
    end

    context "referencing a component using a wrong ref value" do
      let!(:component) { create :component_dummy, ref: "capiroto" }

      before do
        subject[subject.tag_containers.first] = "[Dummy ref:capiroto-doido]"
        subject.save
      end

      it "fails the validation" do
        expect(subject.errors.messages).to include(
          base: ['componente "Dummy" (ref: "capiroto-doido") não encontrado.']
        )
      end
    end
  end

  context "on destroy" do
    context "with components on it's tag container" do
      let(:component) { create(:component_dummy) }

      before do
        subject[subject.tag_containers.first] = component.to_s
        subject.save!
      end

      context "adding the same component" do
        let(:old_relation_count) do
          Relationship.where(from_relatable: subject).count
        end

        let(:new_relation_count) do
          Relationship.where(from_relatable: subject).count
        end

        before do
          old_relation_count
          subject[subject.tag_containers.first] += component.to_s
          subject.save!
          new_relation_count
        end

        it "doesn't create a new relationship" do
          expect(old_relation_count).to eq new_relation_count
        end
      end

      context "with duplicated component" do
        before do
          subject[subject.tag_containers.first] += component.to_s
          subject.save!
        end

        context "removing duplicated" do
          let(:old_relation_count) do
            Relationship.where(from_relatable: subject).count
          end

          let(:new_relation_count) do
            Relationship.where(from_relatable: subject).count
          end

          before do
            old_relation_count

            subject[subject.tag_containers.first]
              .sub! "#{component}#{component}", component.to_s

            subject.save!
            new_relation_count
          end

          it "doesn't destroy any relationship" do
            expect(old_relation_count).to eq new_relation_count
          end
        end
      end
    end
  end
end
