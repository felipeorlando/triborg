module RailsAdminHelper
  def fill_autocomplete_select(id, index: 0)
    page.execute_script <<~JAVASCRIPT
      var query = "[data-input-for='#{id}'] .input-group-btn";
      document.querySelectorAll(query)[0].click();
      document.querySelectorAll(".ui-autocomplete li a")[#{index}].click();
    JAVASCRIPT
  end

  def fill_in_ckeditor(instance_id, with:)
    page.execute_script <<~JAVASCRIPT
      var ckeditor = CKEDITOR.instances['#{instance_id}']
      ckeditor.setData('#{with}')
      ckeditor.focus()
      ckeditor.updateElement()
    JAVASCRIPT
  end
end
