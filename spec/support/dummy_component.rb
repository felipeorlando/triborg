RSpec.configure do |config|
  connection = ActiveRecord::Base.connection
  table_name = Component::Dummy.table_name

  config.before(:suite) do
    connection.create_table table_name do |t|
      t.string :ref
      t.text :body
      t.datetime :deleted_at
    end
  end

  config.after(:suite) { connection.drop_table table_name }
end
