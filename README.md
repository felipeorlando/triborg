# Triborg

[![Build Status](https://semaphoreci.com/api/v1/projects/3d40e9f5-5b48-4088-a216-b2a60ae23961/1121452/badge.svg)](https://semaphoreci.com/getninjas/triborg)


## Dependências

* Ruby 2.3.3
* MySQL
* PhantomJS (2.1.1 ou superior)
* NodeJS ou algo como [therubyracer](https://github.com/cowboyd/therubyracer) para rodar o asset precompile


## Setup

Criar os YAMLs de configuração (configure a seu gosto :-))

```
./bin/setup
```

Instale as gem

```
bundle install
```

Configure o banco de dados

```
bundle exec rails db:setup
```

Rode a data migration "db/data_migration/import_categories.rb" para importar as categorias

```
rails console
require "./db/data_migration/import_categories.rb"
ImportCategories.new.execute
```
