desc "Remove grammarly-btn tag from any tag container"
@logger = ActiveSupport::Logger
         .new(Rails.root.join("log/clear_grammarly.log"))

task clear_grammarly: :environment do

  @logger.info "Rake just started at #{Time.now}"

  begin
    remove_from_components
    remove_from_page_services
  rescue => error
    @logger.error error
  end

  @logger.info "Rake just finished at #{Time.now}"
end

def remove_from_components
  classes = module_classes Component
  classes.each do |klass|
    components = Component.class_eval(klass.to_s).all
    clear_containers components
  end
end

def remove_from_page_services
  services_pages = Page::Service.all
  clear_containers services_pages
end

def clear_containers(compoenents)
  compoenents.each do |component|
    tag_containers = component.tag_containers

    tag_containers.each do |tag|
      remove_grammarly_tag! component.send(tag)
    end

    next if component.save

    @logger.error "Was not possible to save #{component.class} "\
    "with id: #{component.id}"

    @logger.error component.errors.messages
  end
end

def module_classes(moduli)
  moduli.constants.select { |klass| moduli.const_get(klass).is_a? Class }
end

def remove_grammarly_tag!(html)
  html.gsub! %r{<grammarly-btn>.*</grammarly-btn>}, ""
end
