Rails.application.routes.draw do
  mount RailsAdmin::Engine, at: "/admin", as: "rails_admin"
  mount Ckeditor::Engine, at: "/ckeditor"
  mount HealthMonitor::Engine, at: "/health"

  get "/:root_category_slug/servicos/:slug",
    to: "services#show",
    as: "show_service"
end
