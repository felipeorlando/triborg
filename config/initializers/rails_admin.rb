# frozen_string_literal: true

RailsAdmin.config do |config|
  DATE_COLUMN_WIDTH = 130
  DATE_COLUMN_FORMAT = "%d/%m/%y %H:%M:%S"
  REF_HELP = "Obrigatório. Use apenas números, letras minúsculas, '/' ou '-'."

  unless Rails.env.test?
    config.authorize_with do
      authenticate_or_request_with_http_basic("Triborg") do |username, password|
        username == "ninja" && password == "jetNinja123"
      end
    end
  end

  config.actions do
    dashboard
    index
    export
    show
    edit

    new do
      except [
        Category,
        Cluster
      ]
    end

    delete do
      except [
        Category,
        Cluster
      ]
    end
  end

  config.included_models = [
    Category,
    Cluster,
    Component::Citation,
    Component::Content,
    Component::DetailedImage,
    Component::Form,
    Component::Generic,
    Component::Header,
    Component::InternalLink,
    Component::PriceVariation,
    Component::Video,
    Page::Service
  ]

  config.model Category do
    object_label_method do
      :slug
    end

    list do
      sort_by :slug

      field :id do
        column_width 50
      end
      field :slug
      field :name
    end

    edit do
      field :header_image, :carrierwave
    end
  end

  config.model Cluster do
    list do
      sort_by :name

      field :id do
        column_width 50
      end
      field :icon do
        pretty_value do
          bindings[:view]
            .tag :img, src: bindings[:object].icon.url, height: "50"
        end
        label "Imagem"
        column_width 76
      end
      field :name do
        column_width 250
      end
      field :alt
    end

    edit do
      field :alt
    end
  end

  config.model Page::Service do
    list do
      sort_by :slug

      field :slug
      field :category
      field :cluster
      field :updated_at do
        column_width DATE_COLUMN_WIDTH
        strftime_format DATE_COLUMN_FORMAT
      end
      field :view_in_app do
        formatted_value do
          bindings[:view].link_to(
            bindings[:view].tag(:i, class: "icon-eye-open", target: "_blank"),
            bindings[:view].main_app.show_service_path(
              bindings[:object].category.slug,
              bindings[:object].slug
            )
          )
        end
        label ""
        column_width 20
      end
    end

    edit do
      field :category do
        associated_collection_scope do
          proc do |scope|
            scope.where(ancestry_depth: 0)
          end
        end
      end
      field :cluster
      field :friendly_name
      field :title
      field :meta_description
      field :meta_keywords
      field :header do
        required false
        css_class "grid-editor-field"
      end
      field :internal_link do
        required false
        css_class "grid-editor-field"
      end
      field :content do
        required false
        css_class "grid-editor-field"
      end
      field :sidebar do
        required false
        css_class "grid-editor-field"
      end
    end
  end

  config.model Component::DetailedImage do
    list do
      sort_by :ref

      field :image do
        pretty_value do
          bindings[:view]
            .tag :img, src: bindings[:object].image.url(:_360x170), height: "50"
        end
        column_width 116
      end
      field :to_s do
        label "Tag"
      end
      field :title
      field :description
      field :updated_at do
        column_width DATE_COLUMN_WIDTH
        strftime_format DATE_COLUMN_FORMAT
      end
    end

    edit do
      field :ref do
        help REF_HELP
      end
      field :title
      field :description, :ck_editor
      field :image, :carrierwave
    end
  end

  config.model Component::PriceVariation do
    list do
      sort_by :ref

      field :to_s do
        label "Tag"
      end
      field :title
      field :cheap do
        column_width 65
      end
      field :average do
        column_width 65
      end
      field :expensive do
        column_width 65
      end
      field :updated_at do
        column_width DATE_COLUMN_WIDTH
        strftime_format DATE_COLUMN_FORMAT
      end
    end

    edit do
      field :ref do
        help REF_HELP
      end
      field :title
      field :cheap
      field :average
      field :expensive
    end
  end

  config.model Component::Citation do
    list do
      sort_by :ref

      field :to_s do
        label "Tag"
      end
      field :name
      field :updated_at do
        column_width DATE_COLUMN_WIDTH
        strftime_format DATE_COLUMN_FORMAT
      end
    end

    edit do
      field :ref do
        help REF_HELP
      end
      field :name
      field :info
      field :body do
        required false
        css_class "grid-editor-field"
      end
    end
  end

  config.model Component::Content do
    list do
      sort_by :ref

      field :to_s do
        label "Tag"
      end
      field :title
      field :updated_at do
        column_width DATE_COLUMN_WIDTH
        strftime_format DATE_COLUMN_FORMAT
      end
    end

    edit do
      field :ref do
        help REF_HELP
      end
      field :title
      field :subtitle
      field :body do
        required false
        css_class "grid-editor-field"
      end
    end
  end

  config.model Component::Generic do
    list do
      sort_by :ref

      field :to_s do
        label "Tag"
      end
      field :updated_at do
        column_width DATE_COLUMN_WIDTH
        strftime_format DATE_COLUMN_FORMAT
      end
    end

    edit do
      field :ref do
        help REF_HELP
      end
      field :body do
        required false
        css_class "grid-editor-field"
      end
    end
  end

  config.model Component::Header do
    list do
      sort_by :ref

      field :to_s do
        label "Tag"
      end
      field :title
      field :updated_at do
        column_width DATE_COLUMN_WIDTH
        strftime_format DATE_COLUMN_FORMAT
      end
    end

    edit do
      field :ref do
        help REF_HELP
      end
      field :title
      field :subtitle
      field :header_image, :carrierwave
    end
  end

  config.model Component::InternalLink do
    list do
      sort_by :ref

      field :image do
        pretty_value do
          bindings[:view]
            .tag :img, src: bindings[:object].image.url, height: "50"
        end
        column_width 116
      end
      field :to_s do
        label "Tag"
      end
      field :title
      field :updated_at do
        column_width DATE_COLUMN_WIDTH
        strftime_format DATE_COLUMN_FORMAT
      end
    end

    edit do
      field :ref do
        help REF_HELP
      end
      field :image, :carrierwave
      field :title
      field :url
    end
  end
  

  config.model Component::Video do
    list do
      sort_by :ref

      field :to_s do
        label "Tag"
      end
      field :url do
        pretty_value do
          bindings[:object].url
        end
      end
      field :updated_at do
        column_width DATE_COLUMN_WIDTH
        strftime_format DATE_COLUMN_FORMAT
      end
    end

    edit do
      field :ref
      field :url do
        formatted_value do
          bindings[:object].url
        end
      end
    end
  end

  config.model Component::Form do
    list do
      sort_by :ref

      field :to_s do
        label "Tag"
      end
      field :category
      field :css_file
      field :updated_at do
        column_width DATE_COLUMN_WIDTH
        strftime_format DATE_COLUMN_FORMAT
      end
    end

    edit do
      field :ref
      field :category
      field :css_file
    end
  end
end
