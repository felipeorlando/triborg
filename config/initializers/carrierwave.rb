bucket_settings = Config::AppSettings.images_bucket

if bucket_settings.enabled
  CarrierWave.configure do |config|
    config.storage    = :aws
    config.aws_bucket = bucket_settings.bucket_name
    config.aws_acl    = "public-read"
    config.asset_host = bucket_settings.asset_host
    config.aws_authenticated_url_expiration = 60 * 60 * 24 * 7
    config.aws_attributes = {
      cache_control: "max-age=#{1.year.to_i}"
    }

    config.aws_credentials = {
      access_key_id:     bucket_settings.access_key_id,
      secret_access_key: bucket_settings.secret_access_key,
      region:            bucket_settings.region
    }
  end
end

module CarrierWave
  module MiniMagick
    def quality(percentage)
      manipulate! do |img|
        img.quality(percentage.to_s)
        img = yield(img) if block_given?
        img
      end
    end
  end
end
