# frozen_string_literal: true

require "aws-sdk"

Aws.config.update region: Config::Aws.region
