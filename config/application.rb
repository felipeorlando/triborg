require_relative "boot"

require "rails/all"

Bundler.require(*Rails.groups)

module Triborg
  class Application < Rails::Application
    config.autoload_paths += %W(
      #{config.root}/app/services
      #{config.root}/app/value_objects
    )

    config.i18n.load_path += Dir[
      Rails.root.join("config/locales/**/*.yml").to_s
    ]
    config.i18n.available_locales = [:"pt-BR"]
    config.i18n.default_locale = :"pt-BR"
  end
end
