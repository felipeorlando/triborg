settings = Config::AppSettings.sitemap

SitemapGenerator::Sitemap.default_host = settings.default_host

if settings.fog_provider.present?
  SitemapGenerator::Sitemap.adapter = \
    SitemapGenerator::S3Adapter.new(
      fog_provider:          settings.fog_provider,
      fog_directory:         settings.fog_directory,
      fog_region:            settings.fog_region,
      aws_access_key_id:     settings.aws_access_key_id,
      aws_secret_access_key: settings.aws_secret_access_key
    )
else
  SitemapGenerator::Sitemap.sitemaps_path = "sitemaps/"
end

SitemapGenerator::Sitemap.create do
  Page::Service.find_each do |service|
    add show_service_path(service.category.slug, service.slug)
  end
end
