env :PATH, ENV["PATH"]

set :output, "log/cron.log"

job_type :runner,
  "cd :path && RAILS_ENV=:environment cronlock bundle exec rails :task :output"

every 1.day, at: "5:00am" do
  runner "sitemap:refresh"
end
