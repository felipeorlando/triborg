# require "./db/data_migration/import_category_clusters.rb"

c = Cluster.find_or_initialize_by id: 1
c.name = 'Advogados'
c.created_at = '2016-11-09 15:12:19 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/1/original/advogados.svg?1480428277'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 2
c.name = 'Arquitetura e Decoração'
c.created_at = '2016-11-09 15:12:20 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/2/original/arquitetura_e_decoracao.svg?1480428277'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 3
c.name = 'Artes'
c.created_at = '2016-11-09 15:12:20 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/3/original/artes.svg?1480428277'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 4
c.name = 'Assistência Técnica'
c.created_at = '2016-11-09 15:12:20 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/4/original/assistencia_tecnica.svg?1480428277'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 5
c.name = 'Audiovisual'
c.created_at = '2016-11-09 15:12:20 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/5/original/audiovisual.svg?1480428277'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 6
c.name = 'Aulas'
c.created_at = '2016-11-09 15:12:21 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/6/original/aulas.svg?1480428278'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 7
c.name = 'Automação Residencial'
c.created_at = '2016-11-09 15:12:21 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/7/original/automacao_residencial.svg?1480428278'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 8
c.name = 'Automóveis'
c.created_at = '2016-11-09 15:12:21 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/8/original/automoveis.svg?1480428278'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 9
c.name = 'Chaveiro'
c.created_at = '2016-11-09 15:12:21 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/9/original/chaveiro.svg?1480428278'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 10
c.name = 'Construção'
c.created_at = '2016-11-09 15:12:21 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/10/original/construcao.svg?1480428278'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 11
c.name = 'Consultoria'
c.created_at = '2016-11-09 15:12:22 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/11/original/consultoria.svg?1480428279'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 12
c.name = 'Danças'
c.created_at = '2016-11-09 15:12:22 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/12/original/dancas.svg?1480428279'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 13
c.name = 'Dedetizadores'
c.created_at = '2016-11-09 15:12:22 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/13/original/dedetizadores.svg?1480428279'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 14
c.name = 'Desenvolvedores'
c.created_at = '2016-11-09 15:12:22 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/14/original/desenvolvedores.svg?1480428279'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 15
c.name = 'Design'
c.created_at = '2016-11-09 15:12:22 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/15/original/design.svg?1480428280'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 16
c.name = 'Encanamentos'
c.created_at = '2016-11-09 15:12:22 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/16/original/encanamentos.svg?1480428280'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 17
c.name = 'Esportes'
c.created_at = '2016-11-09 15:12:22 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/17/original/esportes.svg?1480428280'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 18
c.name = 'Estética'
c.created_at = '2016-11-09 15:12:23 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/18/original/estetica.svg?1480428280'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 19
c.name = 'Eventos'
c.created_at = '2016-11-09 15:12:23 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/19/original/eventos.svg?1480428281'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 20
c.name = 'Finanças'
c.created_at = '2016-11-09 15:12:23 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/20/original/financas.svg?1480428281'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 21
c.name = 'Idiomas'
c.created_at = '2016-11-09 15:12:23 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/21/original/idiomas.svg?1480428281'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 22
c.name = 'Marcenaria'
c.created_at = '2016-11-09 15:12:23 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/22/original/marcenaria.svg?1480428281'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 23
c.name = 'Marido de Aluguel'
c.created_at = '2016-11-09 15:12:23 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/23/original/marido_de_aluguel.svg?1480428282'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 24
c.name = 'Moda e Beleza'
c.created_at = '2016-11-09 15:12:23 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/24/original/moda_e_beleza.svg?1480428282'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 25
c.name = 'Montadores de Móveis'
c.created_at = '2016-11-09 15:12:24 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/25/original/montadores_de_moveis.svg?1480428282'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 26
c.name = 'Mudanças e Carretos'
c.created_at = '2016-11-09 15:12:24 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/26/original/mudancas_e_carretos.svg?1480428282'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 27
c.name = 'Música'
c.created_at = '2016-11-09 15:12:24 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/27/original/musica.svg?1480428282'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 28
c.name = 'Paisagismo'
c.created_at = '2016-11-09 15:12:24 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/28/original/paisagismo.svg?1480428283'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 29
c.name = 'Pets'
c.created_at = '2016-11-09 15:12:24 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/29/original/pets.svg?1480428283'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 30
c.name = 'Piscinas'
c.created_at = '2016-11-09 15:12:24 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/30/original/piscinas.svg?1480428283'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 31
c.name = 'Reformas e Reparos'
c.created_at = '2016-11-09 15:12:24 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/31/original/reformas_e_reparos.svg?1480428283'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 32
c.name = 'Advogados'
c.created_at = '2016-11-09 15:12:24 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/32/original/advogados.svg?1480428277'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 33
c.name = 'Saúde'
c.created_at = '2016-11-09 15:12:24 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/33/original/saude.svg?1480428283'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 34
c.name = 'Serralheria'
c.created_at = '2016-11-09 15:12:25 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/34/original/serralheria.svg?1480428284'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 35
c.name = 'Arquitetura e Decoração'
c.created_at = '2016-11-09 15:12:25 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/35/original/arquitetura_e_decoracao.svg?1480428277'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 36
c.name = 'Serviços Domésticos'
c.created_at = '2016-11-09 15:12:25 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/36/original/servicos_domesticos.svg?1480428284'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 37
c.name = 'Artes'
c.created_at = '2016-11-09 15:12:25 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/37/original/artes.svg?1480428277'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 38
c.name = 'Serviços Elétricos'
c.created_at = '2016-11-09 15:12:25 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/38/original/servicos_eletricos.svg?1480428284'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 39
c.name = 'Assistência Técnica'
c.created_at = '2016-11-09 15:12:25 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/39/original/assistencia_tecnica.svg?1480428277'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 40
c.name = 'Audiovisual'
c.created_at = '2016-11-09 15:12:25 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/40/original/audiovisual.svg?1480428277'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 41
c.name = 'Aulas'
c.created_at = '2016-11-09 15:12:25 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/41/original/aulas.svg?1480428278'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 42
c.name = 'Automação Residencial'
c.created_at = '2016-11-09 15:12:25 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/42/original/automacao_residencial.svg?1480428278'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 43
c.name = 'Automóveis'
c.created_at = '2016-11-09 15:12:26 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/43/original/automoveis.svg?1480428278'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 44
c.name = 'Chaveiro'
c.created_at = '2016-11-09 15:12:26 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/44/original/chaveiro.svg?1480428278'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 45
c.name = 'Construção'
c.created_at = '2016-11-09 15:12:26 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/45/original/construcao.svg?1480428279'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 46
c.name = 'Serviços Pessoais'
c.created_at = '2016-11-09 15:12:27 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/46/original/servicos_pessoais.svg?1480428284'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 47
c.name = 'Tapeçaria'
c.created_at = '2016-11-09 15:12:27 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/47/original/tapecaria.svg?1480428284'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 48
c.name = 'Tradução'
c.created_at = '2016-11-09 15:12:27 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/48/original/traducao.svg?1480428285'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 49
c.name = 'Consultoria'
c.created_at = '2016-11-09 15:12:27 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/49/original/consultoria.svg?1480428279'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 50
c.name = 'Vidraçaria'
c.created_at = '2016-11-09 15:12:27 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/50/original/vidracaria.svg?1480428285'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 51
c.name = 'Família'
c.created_at = '2016-11-09 15:12:27 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/51/original/familia.svg?1480428285'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 52
c.name = 'Danças'
c.created_at = '2016-11-09 15:12:27 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/52/original/dancas.svg?1480428279'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 53
c.name = 'Design e Tecnologia'
c.created_at = '2016-11-09 15:12:27 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/53/original/design_e_tecnologia.svg?1480428285'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 54
c.name = 'Dedetizadores'
c.created_at = '2016-11-09 15:12:28 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/54/original/dedetizadores.svg?1480428279'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 55
c.name = 'Desenvolvedores'
c.created_at = '2016-11-09 15:12:28 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/55/original/desenvolvedores.svg?1480428279'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 56
c.name = 'Design'
c.created_at = '2016-11-09 15:12:28 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/56/original/design.svg?1480428280'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 57
c.name = 'Encanamentos'
c.created_at = '2016-11-09 15:12:30 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/57/original/encanamentos.svg?1480428280'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 58
c.name = 'Esportes'
c.created_at = '2016-11-09 15:12:30 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/58/original/esportes.svg?1480428280'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 59
c.name = 'Estética'
c.created_at = '2016-11-09 15:12:30 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/59/original/estetica.svg?1480428280'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 60
c.name = 'Eventos'
c.created_at = '2016-11-09 15:12:30 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/60/original/eventos.svg?1480428281'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 61
c.name = 'Finanças'
c.created_at = '2016-11-09 15:12:30 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/61/original/financas.svg?1480428281'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 62
c.name = 'Idiomas'
c.created_at = '2016-11-09 15:12:31 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/62/original/idiomas.svg?1480428281'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 63
c.name = 'Marcenaria'
c.created_at = '2016-11-09 15:12:31 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/63/original/marcenaria.svg?1480428281'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 64
c.name = 'Marido de Aluguel'
c.created_at = '2016-11-09 15:12:31 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/64/original/marido_de_aluguel.svg?1480428282'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 65
c.name = 'Moda e Beleza'
c.created_at = '2016-11-09 15:12:31 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/65/original/moda_e_beleza.svg?1480428282'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 66
c.name = 'Montadores de Móveis'
c.created_at = '2016-11-09 15:12:32 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/66/original/montadores_de_moveis.svg?1480428282'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 67
c.name = 'Mudanças e Carretos'
c.created_at = '2016-11-09 15:12:32 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/67/original/mudancas_e_carretos.svg?1480428282'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 68
c.name = 'Música'
c.created_at = '2016-11-09 15:12:32 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/68/original/musica.svg?1480428282'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 69
c.name = 'Paisagismo'
c.created_at = '2016-11-09 15:12:32 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/69/original/paisagismo.svg?1480428283'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 70
c.name = 'Pets'
c.created_at = '2016-11-09 15:12:32 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/70/original/pets.svg?1480428283'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 71
c.name = 'Piscinas'
c.created_at = '2016-11-09 15:12:32 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/71/original/piscinas.svg?1480428283'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 72
c.name = 'Reformas e Reparos'
c.created_at = '2016-11-09 15:12:32 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/72/original/reformas_e_reparos.svg?1480428283'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 73
c.name = 'Saúde'
c.created_at = '2016-11-09 15:12:33 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/73/original/saude.svg?1480428284'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 74
c.name = 'Serralheria'
c.created_at = '2016-11-09 15:12:33 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/74/original/serralheria.svg?1480428284'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 75
c.name = 'Serviços Domésticos'
c.created_at = '2016-11-09 15:12:33 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/75/original/servicos_domesticos.svg?1480428284'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 76
c.name = 'Serviços Elétricos'
c.created_at = '2016-11-09 15:12:33 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/76/original/servicos_eletricos.svg?1480428284'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 77
c.name = 'Serviços Pessoais'
c.created_at = '2016-11-09 15:12:33 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/77/original/servicos_pessoais.svg?1480428284'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 78
c.name = 'Tapeçaria'
c.created_at = '2016-11-09 15:12:33 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/78/original/tapecaria.svg?1480428285'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 79
c.name = 'Tradução'
c.created_at = '2016-11-09 15:12:33 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/79/original/traducao.svg?1480428285'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 80
c.name = 'Vidraçaria'
c.created_at = '2016-11-09 15:12:33 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/80/original/vidracaria.svg?1480428285'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 81
c.name = 'Família'
c.created_at = '2016-11-09 15:12:34 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/81/original/familia.svg?1480428285'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close

c = Cluster.find_or_initialize_by id: 82
c.name = 'Design e Tecnologia'
c.created_at = '2016-11-09 15:12:34 -0200'
c.alt = 'alt_teste'
a = open(URI.parse('https://images.getninjas.com.br/icons/82/original/design_e_tecnologia.svg?1480428285'))
file = Tempfile.new(['tmp', '.svg']).binmode
file.write a.read
c.icon.store! file
c.save
file.close
