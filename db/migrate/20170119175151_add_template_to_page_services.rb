class AddTemplateToPageServices < ActiveRecord::Migration[5.0]
  def change
    add_column :page_services, :header, :text
    add_column :page_services, :sidebar, :text
  end
end
