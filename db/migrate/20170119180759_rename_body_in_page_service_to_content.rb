class RenameBodyInPageServiceToContent < ActiveRecord::Migration[5.0]
  def change
    rename_column :page_services, :body, :content
  end
end
