class RenameClustersIconFileNameToIcon < ActiveRecord::Migration[5.0]
  def change
    rename_column :clusters, :icon_file_name, :icon
  end
end
