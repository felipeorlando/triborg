class CreateComponentDetailedImages < ActiveRecord::Migration[5.0]
  def change
    create_table :component_detailed_images do |t|
      t.string :ref,         null: false, limit: 100
      t.string :title,       null: false
      t.string :image,       null: false
      t.text   :description, null: false
      t.string :image_filename
      t.string :image_extension
      t.datetime :deleted_at

      t.timestamps null: false
    end
    add_index :component_detailed_images, :ref
    add_index :component_detailed_images, :deleted_at
  end
end
