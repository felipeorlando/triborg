class DropVariablesTable < ActiveRecord::Migration[5.0]
  def change
    drop_table :variable_values, {}
    drop_table :variables, {}
  end
end
