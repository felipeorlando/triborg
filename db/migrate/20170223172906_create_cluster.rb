class CreateCluster < ActiveRecord::Migration[5.0]
  def change
    create_table :clusters do |t|
      t.string :name, null: false
      t.string :icon_file_name, null: false
      t.string :icon_file_extension
      t.string :alt
      t.timestamps null: false
      t.datetime :deleted_at
    end
  end
end
