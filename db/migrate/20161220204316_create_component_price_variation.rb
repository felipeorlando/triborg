class CreateComponentPriceVariation < ActiveRecord::Migration[5.0]
  def change
    create_table :component_price_variations do |t|
      t.string :ref,        null: false, limit: 100
      t.string :title,      null: false
      t.decimal :cheap,     null: false, precision: 10, scale: 2
      t.decimal :average,   null: false, precision: 10, scale: 2
      t.decimal :expensive, null: false, precision: 10, scale: 2
      t.timestamps          null: false
      t.datetime :deleted_at
    end

    add_index :component_price_variations, :ref
    add_index :component_price_variations, :deleted_at
  end
end
