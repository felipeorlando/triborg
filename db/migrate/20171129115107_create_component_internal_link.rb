class CreateComponentInternalLink < ActiveRecord::Migration[5.0]
  def change
    create_table :component_internal_links do |t|
      t.string   :ref,   null: false, limit: 100
      t.string   :image, null: false
      t.string   :title, null: false, limit: 45
      t.string   :url,   null: false
      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :component_internal_links, :ref
    add_index :component_internal_links, :deleted_at
  end
end
