class AddClustersRefToPageServices < ActiveRecord::Migration[5.0]
  def change
    add_reference :page_services, :cluster, foreign_key: true
  end
end
