class CreateComponentForm < ActiveRecord::Migration[5.0]
  def change
    create_table :component_forms do |t|
      t.references :category, null: false
      t.string :css_file
      t.string :ref, null: false, limit: 100

      t.timestamps null: false
      t.datetime :deleted_at
    end

    add_index :component_forms, :ref
    add_index :component_forms, :deleted_at
  end
end
