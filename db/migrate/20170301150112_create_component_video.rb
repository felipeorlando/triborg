class CreateComponentVideo < ActiveRecord::Migration[5.0]
  def change
    create_table :component_videos do |t|
      t.string :ref, null: false, limit: 100
      t.string :video_id, null: false, limit: 20
      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :component_videos, :ref
    add_index :component_videos, :deleted_at
  end
end
