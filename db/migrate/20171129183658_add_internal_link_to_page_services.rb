class AddInternalLinkToPageServices < ActiveRecord::Migration[5.0]
  def change
    add_column :page_services, :internal_link, :text, after: :header
  end
end
