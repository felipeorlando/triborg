# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171129183658) do

  create_table "categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "ancestry"
    t.integer  "ancestry_depth",         default: 0, null: false
    t.string   "slug",                               null: false
    t.string   "name",                               null: false
    t.string   "friendly_name",                      null: false
    t.string   "header_image"
    t.string   "header_image_filename"
    t.string   "header_image_extension"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.index ["ancestry"], name: "index_categories_on_ancestry", using: :btree
    t.index ["ancestry_depth"], name: "index_categories_on_ancestry_depth", using: :btree
    t.index ["slug"], name: "index_categories_on_slug", using: :btree
  end

  create_table "ckeditor_assets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
    t.index ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree
  end

  create_table "clusters", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name",                null: false
    t.string   "icon",                null: false
    t.string   "icon_file_extension"
    t.string   "alt"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.datetime "deleted_at"
  end

  create_table "component_citations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "ref",        limit: 100,   null: false
    t.string   "name"
    t.string   "info"
    t.text     "body",       limit: 65535, null: false
    t.datetime "deleted_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["deleted_at"], name: "index_component_citations_on_deleted_at", using: :btree
    t.index ["ref"], name: "index_component_citations_on_ref", using: :btree
  end

  create_table "component_contents", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "ref",        limit: 100,   null: false
    t.string   "title"
    t.string   "subtitle"
    t.integer  "background"
    t.text     "body",       limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["deleted_at"], name: "index_component_contents_on_deleted_at", using: :btree
    t.index ["ref"], name: "index_component_contents_on_ref", using: :btree
  end

  create_table "component_detailed_images", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "ref",             limit: 100,   null: false
    t.string   "title",                         null: false
    t.string   "image",                         null: false
    t.text     "description",     limit: 65535, null: false
    t.string   "image_filename"
    t.string   "image_extension"
    t.datetime "deleted_at"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["deleted_at"], name: "index_component_detailed_images_on_deleted_at", using: :btree
    t.index ["ref"], name: "index_component_detailed_images_on_ref", using: :btree
  end

  create_table "component_forms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "category_id",             null: false
    t.string   "css_file"
    t.string   "ref",         limit: 100, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.datetime "deleted_at"
    t.index ["category_id"], name: "index_component_forms_on_category_id", using: :btree
    t.index ["deleted_at"], name: "index_component_forms_on_deleted_at", using: :btree
    t.index ["ref"], name: "index_component_forms_on_ref", using: :btree
  end

  create_table "component_generics", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "ref",        limit: 100,   null: false
    t.text     "body",       limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["deleted_at"], name: "index_component_generics_on_deleted_at", using: :btree
    t.index ["ref"], name: "index_component_generics_on_ref", using: :btree
  end

  create_table "component_headers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "ref",                    limit: 100, null: false
    t.string   "title"
    t.string   "subtitle"
    t.string   "header_image"
    t.string   "header_image_filename"
    t.string   "header_image_extension"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_component_headers_on_deleted_at", using: :btree
    t.index ["ref"], name: "index_component_headers_on_ref", using: :btree
  end

  create_table "component_internal_links", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "ref",        limit: 100, null: false
    t.string   "image",                  null: false
    t.string   "title",      limit: 45,  null: false
    t.string   "url",                    null: false
    t.datetime "deleted_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["deleted_at"], name: "index_component_internal_links_on_deleted_at", using: :btree
    t.index ["ref"], name: "index_component_internal_links_on_ref", using: :btree
  end

  create_table "component_price_variations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "ref",        limit: 100,                          null: false
    t.string   "title",                                           null: false
    t.decimal  "cheap",                  precision: 10, scale: 2, null: false
    t.decimal  "average",                precision: 10, scale: 2, null: false
    t.decimal  "expensive",              precision: 10, scale: 2, null: false
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_component_price_variations_on_deleted_at", using: :btree
    t.index ["ref"], name: "index_component_price_variations_on_ref", using: :btree
  end

  create_table "component_videos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "ref",        limit: 100, null: false
    t.string   "video_id",   limit: 20,  null: false
    t.datetime "deleted_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["deleted_at"], name: "index_component_videos_on_deleted_at", using: :btree
    t.index ["ref"], name: "index_component_videos_on_ref", using: :btree
  end

  create_table "friendly_id_slugs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree
  end

  create_table "page_service_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "service_id"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["category_id", "service_id"], name: "index_pag_serv_cats_cats_serv", using: :btree
    t.index ["service_id", "category_id"], name: "index_pag_serv_cats_serv_cats", using: :btree
  end

  create_table "page_services", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "category_id",                    null: false
    t.string   "title",                          null: false
    t.string   "slug",                           null: false
    t.string   "friendly_name"
    t.string   "short_name"
    t.string   "meta_description"
    t.string   "meta_keywords"
    t.text     "content",          limit: 65535
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.datetime "deleted_at"
    t.text     "header",           limit: 65535
    t.text     "internal_link",    limit: 65535
    t.text     "sidebar",          limit: 65535
    t.integer  "cluster_id"
    t.index ["category_id"], name: "index_page_services_on_category_id", using: :btree
    t.index ["cluster_id"], name: "index_page_services_on_cluster_id", using: :btree
    t.index ["deleted_at"], name: "index_page_services_on_deleted_at", using: :btree
    t.index ["slug"], name: "index_page_services_on_slug", using: :btree
  end

  create_table "relationships", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "from_relatable_type"
    t.integer  "from_relatable_id"
    t.string   "to_relatable_type"
    t.integer  "to_relatable_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["from_relatable_type", "from_relatable_id"], name: "index_relationships_on_from_relatable_type_and_from_relatable_id", using: :btree
    t.index ["to_relatable_type", "to_relatable_id"], name: "index_relationships_on_to_relatable_type_and_to_relatable_id", using: :btree
  end

  create_table "versions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string   "item_type",      limit: 191,        null: false
    t.integer  "item_id",                           null: false
    t.string   "event",                             null: false
    t.string   "whodunnit"
    t.text     "object",         limit: 4294967295
    t.text     "object_changes", limit: 4294967295
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  end

  add_foreign_key "page_service_categories", "categories"
  add_foreign_key "page_service_categories", "page_services", column: "service_id"
  add_foreign_key "page_services", "categories"
  add_foreign_key "page_services", "clusters"
end
